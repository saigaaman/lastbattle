package lastBattle;

import java.util.List;

public class Arisa extends Player{//愛莉紗を管理するクラス。
	//プレイヤーの特徴を受け継いでいる。
	private String name="愛莉紗";
	private int select=0;
	private int skillNum;
	private boolean skillStandby=false;
	/* (非 Javadoc)
	 * @see lastBattle.Player#goSelect(int)
	 */
	//行動の選択をさせるメソッド
	public int goSelect(int select) throws InterruptedException {
		System.out.println(getName()+"『坊ちゃま、ご指示をお願いします』");
		System.out.println(getName()+"の行動を選択してください。");
		Thread.sleep(1000);
		System.out.println("【1】通常攻撃【2】スキル【3】射撃【4】防御【5】アイテム");
		select=new java.util.Scanner(System.in).nextInt();
		return select;
	}



	/* (非 Javadoc)
	 * @see lastBattle.Player#goAttack(java.util.List)
	 */
	//通常攻撃もしくは指定型スキルの際呼ばれるメソッド
	public int goAttack(List<Enemy> enemy) throws InterruptedException{
		boolean selectAttack=false;
		int target = 0;
		while(selectAttack==false) {
			for(int i=0; i<enemy.size(); i++) {//敵の数だけ名前とともに並べる
				System.out.println("【"+i+"】"+enemy.get(i).getName());
				Thread.sleep(1000);
			}
			System.out.println("対象を選んで下さい");
			System.out.println("もし、最初の選択肢に戻る場合は【10】を選択して下さい。");
			target=new java.util.Scanner(System.in).nextInt();
			if(target==10) {//最初の選択肢に戻りたい場合
				selectAttack=true;
			}
			if(target>enemy.size()){//もし敵数より多い数字を入力していたら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			}else if(target==0||target==1||target==2||target==3) {//敵の数が一致したら番号を確認
				selectAttack=true;//敵選択完了
			}else {//それ以外の文字や、入力何もしなかったら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			}
		}//
		return target;
	}

	/* (非 Javadoc)
	 * @see lastBattle.Player#goSkill()
	 */
	//スキル選択メソッド
	public int goSkill() throws InterruptedException{
		skillStandby=false;//バトルリザルトフラグをリセット
		select=0;//セレクトをリセット
		skillNum=0;//スキル番号をリセット
		while(skillStandby==false) {//スキルを決めてから、スキルの効果を実行するまでループ
			System.out.println(getName()+"『どのスキルを使いましょう？』");
			Thread.sleep(1000);
			////////愛莉紗スキル
			System.out.println("1『壱ノ太刀　風刃』　威力B　敵全体に２回連続攻撃＋遅延20");
			Thread.sleep(500);
			System.out.println("2『弐ノ太刀　羅刹』　威力A　自己強化スキル　2回まで直接攻撃回避＆カウンター");
			Thread.sleep(500);
			System.out.println("3『涼風』　　　　　　味方全体のHPを30%回復＋味方の行動速度を少し早める");
			Thread.sleep(500);
			System.out.println("4『参ノ太刀　災禍』　威力SS 敵一体にクリティカル攻撃し、稀に猛毒状態にする(猛毒は未実装)");
			Thread.sleep(500);
			System.out.println("5『ファイナル弁当』　チート技。正常なプレイでは選択しないでください。");
			Thread.sleep(500);
			System.out.println("上記何れかの番号を入力してください。");
			Thread.sleep(500);
			System.out.println("もし、最初の選択肢に戻る場合は【10】を選択して下さい。");
			select=new java.util.Scanner(System.in).nextInt();
			if(select==1) {//もし風刃を選んでいたら
				skillNum=11;
				skillStandby=true;
			}else if(select==2) {//もし羅刹を選んでいたら
				skillNum=12;
				skillStandby=true;
			}else if(select==3) {//もし涼風を選んでいたら
				skillNum=13;
				skillStandby=true;
			}else if(select==4) {//もし災禍を選んでいたら
				skillNum=14;
				skillStandby=true;
			}else if(select==5) {//もしファイナル弁当を選んでいたら
				skillNum=15;
				skillStandby=true;
			}else if(select==10){//もし戻るを選んでいたら
				skillNum=10;
				System.out.println(getName()+"『坊ちゃま、再考致しましょう。』");
				Thread.sleep(1000);
				skillStandby=true;
			}else {
				System.out.println(getName()+"『坊ちゃま、ちゃんと選んで下さい。』");
				Thread.sleep(1000);
			}
		}
		if(select!=10) {
			System.out.println(getName()+"『坊ちゃまのご指示を、承りました。』");
			Thread.sleep(1000);
		}
		return skillNum;//スキルナンバーを返す。
	}

	/* (非 Javadoc)
	 * @see lastBattle.Player#goSupport(java.util.List)
	 */
	//指定型サポートスキル
	public int goSupport(List<Player> player) throws InterruptedException{//指定型サポートスキルで呼ばれるメソッド。
		boolean selectSupport=false;
		int target =0;
		while(selectSupport==false) {
			for(int i=0; i<player.size(); i++) {//敵の数だけ名前とともに並べる
				System.out.println("【"+i+"】"+player.get(i).getName());
				Thread.sleep(1000);
			}
			System.out.println("対象を選んで下さい");
			System.out.println("もし、最初の選択肢に戻る場合は【10】を選択して下さい。");
			target=new java.util.Scanner(System.in).nextInt();
			if(target==10) {//最初の選択肢に戻りたい場合
				selectSupport=true;
			}
			else if(target>player.size()){//もし敵数より多い数字を入力していたら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			}else if(target==0||target==1||target==2||target==3) {//敵の数が一致したら番号を確認
				selectSupport=true;//選択完了
			}else {//それ以外の文字や、入力何もしなかったら
				System.out.println("正しい対象を選択してください");
			}
		}
		return target;
	}

	/* (非 Javadoc)
	 * @see lastBattle.Player#goCounter()
	 */
	//カウンター発動時のスクリプト
	public void goCounter() throws InterruptedException {
		System.out.println(this.name+"『そんな攻撃、当たりません！』");
		Thread.sleep(1500);
		System.out.println(this.name+"は攻撃を回避！");
		Thread.sleep(1500);
		System.out.println(this.name+"『これはお返しです……えいっ！』");
		Thread.sleep(1500);
	}
	/* (非 Javadoc)
	 * @see lastBattle.Player#toAvoidance()
	 */
	//回避時スクリプト
	public void toAvoidance() {
		System.out.println(this.name+"『そんな攻撃、当たりません！』");
	}


	/* (非 Javadoc)
	 * @see lastBattle.Player#attackScript()
	 */
	//通常攻撃のスクリプト
	public void attackScript() throws InterruptedException {
		System.out.println(this.name+"『はあっ！えいっ！』");
		Thread.sleep(1000);
		System.out.println(this.name+"の通常攻撃！");
		Thread.sleep(1000);
	}

	public void breakGo(){//オーブ破壊キャラ選択する際のセリフ
		System.out.println(this.name+"『愛莉紗にお任せください。』");
	}

	public void breakGo2() {//オーブ破壊キャラになった際のセリフ
		System.out.println(this.name+"『畏まりました。必ずや、破壊してみせましょう。』");

	}
	public void orbBreak() {//オーブ破壊セリフ
		System.out.println(this.name+"『後ろががら空きです、お覚悟を！』");

	}

	public void goShot(){//後回し
		//残弾数
		//anyで連発
		//stopで停止
		//stop後行動を戻す
	}

	public void goGuird(){//後回し

	}

	public void goItem(){//後回し

	}

	/**
	 * @param name
	 */
	public Arisa(String name) {//コンストラクタ
		super("愛莉紗");
		this.setcNum(1);
		this.name="愛莉紗";
		this.setLevel(68);
		this.setMaxHp(6850);
		this.setMaxSp(3820);
		this.setHp(getMaxHp());
		this.setSp(getMaxSp());
		this.setAttack(1020);
		this.setMaAttack(985);
		this.setDefense(870);
		this.setMaDefense(970);
		this.setCri(30);//クリティカル率
		this.setAvoid(30);//回避率（％）
		this.setSpeed(80);//最大100
		this.setTurn(0);
		this.setDefBreak(15);//最大100
		this.setAmmo(25);//弾薬
		//ここから特殊状態
		this.setLimitBreak(0);//100でリミットブレイク発動、特定行動で消費
		this.setExtension(false);//高揚状態。キャラごとの条件で発動、ステータス大幅アップ

		//ここから状態異常
		this.setFireDamage(false);
		this.setIceBind(false);
		this.setStan(false);
		this.setSilent(false);
		//愛莉紗作製ここまで

	}

}
