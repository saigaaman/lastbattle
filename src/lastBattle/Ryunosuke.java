package lastBattle;

import java.util.List;

public class Ryunosuke extends Player{//龍之介管理メソッド。
	//内にはプレイヤー型を秘めている
	private String name="龍之介";
	private int select=0;
	private int skillNum;
	private boolean skillStandby=false;
	private boolean kongou=false;//金剛のオンオフ

	/* (非 Javadoc)
	 * @see lastBattle.Player#goSelect(int)
	 */
	//行動選択時のメソッド
	public int goSelect(int select) throws InterruptedException {
		System.out.println(getName()+"『了解。どうすればいい？』");
		Thread.sleep(1000);
		System.out.println(getName()+"の行動を選択してください。");
		Thread.sleep(1000);
		System.out.println("【1】通常攻撃【2】スキル【3】射撃【4】防御【5】アイテム");
		select=new java.util.Scanner(System.in).nextInt();
		return select;
	}


	/* (非 Javadoc)
	 * @see lastBattle.Player#goAttack(java.util.List)
	 */
	//通常攻撃選択時のメソッド
	public int goAttack(List<Enemy> enemy) throws InterruptedException{
		boolean selectAttack=false;
		int target = 0;
		while(selectAttack==false) {
			for(int i=0; i<enemy.size(); i++) {//敵の数だけ名前とともに並べる
				System.out.println("【"+i+"】"+enemy.get(i).getName());
				Thread.sleep(1000);
			}
			System.out.println("対象を選んで下さい");
			System.out.println("もし、最初の選択肢に戻る場合は【10】を選択して下さい。");
			target=new java.util.Scanner(System.in).nextInt();
			if(target==10) {//最初の選択肢に戻りたい場合
				selectAttack=true;
			}
			if(target>enemy.size()){//もし敵数より多い数字を入力していたら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			}else if(target==0||target==1||target==2||target==3) {//敵の数が一致したら番号を確認
				selectAttack=true;//敵選択完了
			}else {//それ以外の文字や、入力何もしなかったら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			}
		}//
		return target;
	}


	/* (非 Javadoc)
	 * @see lastBattle.Player#goSupport(java.util.List)
	 */
	//単体サポートスキルで呼ばれるメソッド
	public int goSupport(List<Player> player) throws InterruptedException{//指定型サポートスキルで呼ばれるメソッド。
		boolean selectSupport=false;
		int target =0;
		while(selectSupport==false) {
			for(int i=0; i<player.size(); i++) {//敵の数だけ名前とともに並べる
				System.out.println("【"+i+"】"+player.get(i).getName());
				Thread.sleep(1000);
			}
			System.out.println("対象を選んで下さい");
			System.out.println("もし、最初の選択肢に戻る場合は【10】を選択して下さい。");
			target=new java.util.Scanner(System.in).nextInt();
			if(target==10) {//最初の選択肢に戻りたい場合
				selectSupport=true;
			}
			else if(target>player.size()){//もし敵数より多い数字を入力していたら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			}else if(target==0||target==1||target==2||target==3) {//敵の数が一致したら番号を確認
				selectSupport=true;//敵選択完了
			}else {//それ以外の文字や、入力何もしなかったら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			}
		}//
		return target;
	}



	/* (非 Javadoc)
	 * @see lastBattle.Player#goSkill()
	 */
	//スキル選択時のメソッド
	public int goSkill() throws InterruptedException{
		skillStandby=false;//バトルリザルトフラグをリセット
		select=0;//セレクトをリセット
		skillNum=0;//スキル番号をリセット
		while(skillStandby==false) {//スキルを決めてから、スキルの効果を実行するまでループ
			System.out.println(getName()+"『スキルを選んでくれ』");
			Thread.sleep(1000);
			////////愛莉紗スキル
			System.out.println("1『徹甲弾』威力A 敵一体にダメージ＋敵装甲を大きく削り取る");
			Thread.sleep(500);
			System.out.println("2『煉獄弾』威力SS 敵全体にマシンガンを乱射する。敵物理防御が低ければダメージ増加");
			Thread.sleep(500);
			System.out.println("3『金剛』　全身を硬化し、味方が戦闘不能となる攻撃を受けた際、一度だけ身代わりとなる（未実装）");
			Thread.sleep(500);
			System.out.println("4『破壊弾』威力S 炸裂する弾頭を使用し、敵全体にダメージ＋確率でスタン付与（スタンは未実装）");
			Thread.sleep(500);
			System.out.println("5『リボルクラッシュ』チート技。正常なプレイでは選択しないで下さい。");
			Thread.sleep(500);
			System.out.println("上記何れかの番号を入力してください。");
			System.out.println("もし、最初の選択肢に戻る場合は【10】を選択して下さい。");
			select=new java.util.Scanner(System.in).nextInt();
			if(select==1) {//もし徹甲弾を選んでいたら
				skillNum=31;
				skillStandby=true;
			}else if(select==2) {//もし速射弾を選んでいたら
				skillNum=32;
				skillStandby=true;
			}else if(select==3) {//もし金剛を選んでいたら
				skillNum=33;
				skillStandby=true;
			}else if(select==4) {//もし破壊弾を選んでいたら
				skillNum=34;
				skillStandby=true;
			}else if(select==5) {//もしリボルクラッシュを選んでいたら
				skillNum=35;
				skillStandby=true;
			}else if(select==10){//もし戻るを選んでいたら
				skillNum=10;
				System.out.println(getName()+"『うむ、ここは選び直そう』");
				Thread.sleep(1000);
				skillStandby=true;
			}else {
				System.out.println(getName()+"『こんな時に冗談を言っている場合ではないぞ？』");
				Thread.sleep(1000);
			}
		}
		if(select!=10) {
			System.out.println(getName()+"『了解した……任せておけ。』");
			Thread.sleep(1000);
		}
		return skillNum;//スキルナンバーを返す。
	}
	//回避時のセリフメソッド
	public void toAvoidance() {
		System.out.println(this.name+"『ふん、この俺を見誤ったようだな……』");
	}

	//通常攻撃時のセリフメソッド
	public void attackScript() throws InterruptedException {
		System.out.println(this.name+"『ふ・・・この銃弾、避けられるかな？』");
		Thread.sleep(1000);
		System.out.println(this.name+"の通常攻撃！");
		Thread.sleep(1000);
	}
	//オーブ破壊
	public void breakGo(){
		System.out.println(this.name+"『俺が行こうか？』");
	}
	//破壊役に選ぶ
	public void breakGo2() {
		System.out.println(this.name+"『了解した、任せておけ。』");
	}
	//破壊時のセリフ
	public void orbBreak() {//オーブ破壊セリフ
		System.out.println(this.name+"『迂闊な動きは、時に命取りとなる・・・』");
	}

	public void goShot(){//後回し
		//残弾数
		//anyで連発
		//stopで停止
		//stop後行動を戻す
	}

	public void goGuird(){//後回し
	}

	public void goItem(){//後回し
	}




	/**
	 * @param name
	 */
	public Ryunosuke(String name) {//コンストラクタ
		super("龍之介");
		this.setcNum(1);
		this.name="龍之介";
		this.setLevel(68);
		this.setMaxHp(7400);
		this.setMaxSp(1800);
		this.setHp(getMaxHp());
		this.setSp(getMaxSp());
		this.setAttack(1120);
		this.setMaAttack(755);
		this.setDefense(970);
		this.setMaDefense(990);
		this.setCri(10);
		this.setAvoid(10);
		this.setSpeed(50);//最大100
		this.setTurn(0);
		this.setDefBreak(50);//最大100
		this.setAmmo(6);//弾薬
		//ここから特殊状態
		this.setLimitBreak(0);//100でリミットブレイク発動、特定行動で消費
		this.setExtension(false);//高揚状態。キャラごとの条件で発動、ステータス大幅アップ
		//ここから状態異常
		this.setFireDamage(false);
		this.setIceBind(false);
		this.setStan(false);
		this.setSilent(false);
		//龍之介作製ここまで
	}


}
