package lastBattle;

import java.util.List;

/**
 * @author 200217AM
 *
 */
public class Siro extends Player {//シロ管理メソッド。
	//内にはプレイヤー型を秘めている
	private String name="シロ";
	private int select=0;
	private int skillNum;
	private boolean skillStandby=false;

	/* (非 Javadoc)
	 * @see lastBattle.Player#goSelect(int)
	 */
	//行動を選択させるメソッド
	public int goSelect(int select) throws InterruptedException {
		System.out.println(getName()+"『何をすればいいのかにゃ？』");
		Thread.sleep(1000);
		System.out.println(getName()+"の行動を選択してください。");
		Thread.sleep(1000);
		System.out.println("【1】通常攻撃【2】スキル【3】射撃【4】防御【5】アイテム");
		select=new java.util.Scanner(System.in).nextInt();
		return select;
	}

	/* (非 Javadoc)
	 * @see lastBattle.Player#goAttack(java.util.List)
	 */
	//通常攻撃or指定型スキルのメソッド
	public int goAttack(List<Enemy> enemy) throws InterruptedException{
		boolean selectAttack=false;
		int target = 0;
		while(selectAttack==false) {
			for(int i=0; i<enemy.size(); i++) {//敵の数だけ名前とともに並べる
				System.out.println("【"+i+"】"+enemy.get(i).getName());
				Thread.sleep(1000);
			}
			System.out.println("対象を選んで下さい");
			System.out.println("もし、最初の選択肢に戻る場合は【10】を選択して下さい。");
			target=new java.util.Scanner(System.in).nextInt();
			if(target==10) {//最初の選択肢に戻りたい場合
				selectAttack=true;
			}
			if(target>enemy.size()){//もし敵数より多い数字を入力していたら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			}else if(target==0||target==1||target==2||target==3) {//敵の数が一致したら番号を確認
				selectAttack=true;//敵選択完了
			}else {//それ以外の文字や、入力何もしなかったら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			}
		}//
		return target;
	}

	/* (非 Javadoc)
	 * @see lastBattle.Player#goSupport(java.util.List)
	 */
	//指定型サポートスキルのメソッド
	public int goSupport(List<Player> player) throws InterruptedException{//指定型サポートスキルで呼ばれるメソッド。
		boolean selectSupport=false;
		int target =0;
		while(selectSupport==false) {
			for(int i=0; i<player.size(); i++) {//敵の数だけ名前とともに並べる
				System.out.println("【"+i+"】"+player.get(i).getName());
				Thread.sleep(1000);
			}
			System.out.println("対象を選んで下さい");
			System.out.println("もし、最初の選択肢に戻る場合は【10】を選択して下さい。");
			target=new java.util.Scanner(System.in).nextInt();
			if(target==10) {//最初の選択肢に戻りたい場合
				selectSupport=true;
			}
			else if(target>player.size()){//もし敵数より多い数字を入力していたら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			}else if(target==0||target==1||target==2||target==3) {//敵の数が一致したら番号を確認
				selectSupport=true;//敵選択完了
			}else {//それ以外の文字や、入力何もしなかったら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			}
		}//
		return target;
	}

	/* (非 Javadoc)
	 * @see lastBattle.Player#goSkill()
	 */
	//スキル選択のメソッド
	public int goSkill() throws InterruptedException{
		skillStandby=false;//バトルリザルトフラグをリセット
		select=0;//セレクトをリセット
		skillNum=0;//スキル番号をリセット
		while(skillStandby==false) {//スキルを決めてから、スキルの効果を実行するまでループ
			System.out.println("シロ『スキルだにゃ。やったるにゃ！』");
			Thread.sleep(1000);
			////////シロスキル
			System.out.println("1『甘える』味方一人のHPを70%回復");
			Thread.sleep(500);
			System.out.println("2『踊る』　味方全体の防御力を5ターン30%上昇させる（未実装）");
			Thread.sleep(500);
			System.out.println("3『投げる』　威力A　光の矢を投げて敵全体にダメージ");
			Thread.sleep(500);
			System.out.println("4『起こす』味方一人の戦闘不能状態を回復（回復量は最大HPの50%）");
			Thread.sleep(500);
			System.out.println("5『スーパー大切断』チート技。正常なプレイでは選択しないで下さい。");
			Thread.sleep(500);
			System.out.println("上記何れかの番号を入力してください。");
			Thread.sleep(500);
			System.out.println("もし、最初の選択肢に戻る場合は【10】を選択して下さい。");
			select=new java.util.Scanner(System.in).nextInt();
			if(select==1) {//もし甘えるを選んでいたら
				skillNum=21;
				skillStandby=true;
			}else if(select==2) {//もし踊るを選んでいたら
				skillNum=22;
				skillStandby=true;
			}else if(select==3) {//もし投げるを選んでいたら
				skillNum=23;
				skillStandby=true;
			}else if(select==4) {//もし起こすを選んでいたら
				skillNum=24;
				skillStandby=true;
			}else if(select==5) {//もしスーパー大切断を選んでいたら
				skillNum=25;
				skillStandby=true;
			}else if(select==10){//もし戻るを選んでいたら
				skillNum=10;
				System.out.println(getName()+"『考え直すのかにゃ？』");
				Thread.sleep(1000);
				skillStandby=true;
			}else {
				System.out.println(getName()+"『ニャーーー！ゴシュジンが何を言っているか分からないニャ！』");
				Thread.sleep(1000);
			}
		}
		if(select!=10) {
			System.out.println(getName()+"『ゴシュジンの言う通りにするニャ！』");
			Thread.sleep(1000);
		}
		return skillNum;//スキルナンバーを返す。
	}

	/* (非 Javadoc)
	 * @see lastBattle.Player#toAvoidance()
	 */
	//回避時のセリフ
	public void toAvoidance() {
		System.out.println(this.name+"『猫のスルー術だにゃ！』");

	}
	//通常攻撃時のセリフ
	public void attackScript() throws InterruptedException {
		System.out.println(this.name+"『にゃ。にゃ。ひっかくにゃ。』");
		Thread.sleep(1000);
		System.out.println(this.name+"の通常攻撃！");
		Thread.sleep(1000);
	}
	//オーブ破壊キャラ選択のセリフ
	public void breakGo(){
		System.out.println(this.name+"『猫に任せるかにゃ？』");
	}
	//オーブ破壊キャラ選択完了のセリフ
	public void breakGo2() {
		System.out.println(this.name+"『おっけー、やるにゃ。』");
	}
	//オーブ破壊時のセリフ
	public void orbBreak() {//オーブ破壊セリフ
		System.out.println(this.name+"『今にゃあああああああああ！』");
	}

	public void goShot(){//後回し
		//残弾数
		//anyで連発
		//stopで停止
		//stop後行動を戻す
	}

	public void goGuird(){//後回し
	}

	public void goItem(){//後回し
	}

	/**
	 * @param name
	 */
	public Siro(String name) {//コンストラクタ
		super("シロ");
		this.setcNum(1);
		this.name="シロ";
		this.setLevel(68);
		this.setMaxHp(5950);
		this.setMaxSp(5860);
		this.setHp(getMaxHp());
		this.setSp(getMaxSp());
		this.setAttack(710);
		this.setMaAttack(1085);
		this.setDefense(873);
		this.setCri(15);//クリティカル率
		this.setAvoid(25);//回避率
		this.setMaDefense(950);
		this.setSpeed(70);//最大100
		this.setTurn(0);
		this.setDefBreak(10);//最大100
		this.setAmmo(30);//弾薬
		//ここから特殊状態
		this.setLimitBreak(0);//100でリミットブレイク発動、特定行動で消費
		this.setExtension(false);//高揚状態。キャラごとの条件で発動、ステータス大幅アップ
		//ここから状態異常
		this.setFireDamage(false);
		this.setIceBind(false);
		this.setStan(false);
		this.setSilent(false);
		//シロ作製ここまで

	}


}
