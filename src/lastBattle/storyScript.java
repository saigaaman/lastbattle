package lastBattle;

import java.util.List;

public class storyScript {
	boolean charaSelect=false;
	int select=0;
	String any;

	/**
	 * @param player
	 * @param breakPlayer
	 * @throws InterruptedException
	 */
	//オーブ破壊キャラを決めるテキスト
	public void battle1Script(List<Player> player, List<Player> breakPlayer) throws InterruptedException {
		System.out.println("幸一『くそ、こうも召喚されたらキリがないぞ！』");
		Thread.sleep(2000);
		System.out.println("シロ『時間も無いのにこれは厳しいにゃあ・・・』");
		Thread.sleep(2000);
		System.out.println("龍之介『ふむ・・・やはりオーブを壊さねばいかんな・・・』");
		Thread.sleep(2000);
		System.out.println("愛莉紗『では一人が回り込み、不意を突いて破壊しましょう』");
		Thread.sleep(2000);
		System.out.println("幸一『では、誰が行く？』");
		Thread.sleep(2000);
		System.out.println("龍之介『決めてくれ、リーダーはお前だ』");
		Thread.sleep(2000);
		while(charaSelect==false){
			for(int i=0; i<player.size();i++){
				if(i==0){//幸一はスルー
					continue;
				}
				player.get(i).breakGo();
				Thread.sleep(2000);
				System.out.println("幸一『オーブ破壊に向かわせたら、しばらくはその者の支援を受けられない・・・』");
				Thread.sleep(2000);
				System.out.println("幸一『誰にオーブを破壊させようか・・・』");
				Thread.sleep(2000);
				System.out.println(player.get(i).getName()+"に任せますか？");
				System.out.println("【0】任せる【1】他のキャラにする");
				select=new java.util.Scanner(System.in).nextInt();
				if(select==0){//選択したらプレイヤーを受け渡しする
					player.get(i).breakGo2();
					charaSelect=true;//選択OK
					breakPlayer.add(player.get(i));//選択したプレイヤーを格納
					player.remove(player.get(i));//選択したプレイヤーがいったん離脱
					System.out.println(breakPlayer.get(0).getName()+"、奇襲の為、一時離脱！");
					Thread.sleep(3000);
					break;
				}else if(select==1){//他のキャラにするなら何もしない
				}
			}
		}
	}

	/**
	 * @param player
	 * @param breakPlayer
	 * @throws InterruptedException
	 */
	//オーブ破壊時のテキスト
	public void battle2Script(List<Player> player,List<Player> breakPlayer) throws InterruptedException {
		player.add(breakPlayer.get(0));//オーブを破壊したプレイヤーが戻ってくる
		breakPlayer.get(0).orbBreak();//オーブ破壊した際のセリフを言う
		System.out.println("黒騎士のオーブが破壊され、召喚不能！");
		Thread.sleep(3000);
		System.out.println(breakPlayer.get(0).getName()+"、パーティーに合流！");
		Thread.sleep(3000);
		breakPlayer.remove(breakPlayer.get(0));//
		Thread.sleep(3000);
		System.out.println("龍之介『これでもう、召喚はできまい！』");
		Thread.sleep(3000);
		System.out.println("愛莉紗『今が好機、ですね』");
		Thread.sleep(3000);
		System.out.println("黒騎士『愚か者どもめ、何れにせよ、ケイオスによりこの都市は破壊される。無駄なことだ！』");
		Thread.sleep(3000);
		System.out.println("幸一『どうかな・・・オーブがなくなっても、あのケイオスが協力してくれるかな？』");
		Thread.sleep(3000);
		System.out.println("シロ『奇跡はきっと起きるにゃあ』");
		Thread.sleep(3000);
		System.out.println("トゥルルルル・・・トゥルルルル");
		Thread.sleep(3000);
		System.out.println("黒騎士『俺だ・・・爆破の準備が出来たのか？』");
		Thread.sleep(3000);
		System.out.println("ケイオス『すまないねぇ、言い忘れていたけどさぁ』");
		Thread.sleep(3000);
		System.out.println("ケイオス『わしの仕掛けはオーブと直結してるのさ』");
		Thread.sleep(3000);
		System.out.println("ケイオス『つまり、オーブが破壊されちゃ、爆破ができないんよ』");
		Thread.sleep(3000);
		System.out.println("黒騎士『貴様、何故そんな重要な事を黙っていた！？』");
		Thread.sleep(3000);
		System.out.println("ケイオス『それに、強力な狙撃手に狙われてたまらんから、逃げるに限るよ』");
		Thread.sleep(3000);
		System.out.println("黒騎士『もしもし、もしもしぃ！？・・・馬鹿な・・・』");
		Thread.sleep(3000);
		System.out.println("龍之介『フン、年貢の納め時ってやつか』");
		Thread.sleep(3000);
		System.out.println("黒騎士『おのれ、貴様らあああああああ！』");
		Thread.sleep(3000);
		System.out.println("シロ『最後の悪あがきが来るにゃあ。』");
		Thread.sleep(3000);
		System.out.println("黒騎士の行動力、増大！");
		Thread.sleep(3000);
		System.out.println("幸一『クイン・シーの援護ももうすぐ来る、みんな、ここが踏ん張りどころだぞ！』");
		Thread.sleep(3000);
		System.out.println("一同『了解！』");
		Thread.sleep(3000);
	}

	/**
	 * @throws InterruptedException
	 */
	public void firstScript() throws InterruptedException {//オープニングテキスト
		System.out.println("この作品は、RPGの最後の戦いを想定して適当にストーリーを作りました");
		System.out.println("【emergency】");
		System.out.println("時折、セリフやスキル名の部分に重度の中二病要素が含まれています");
		System.out.println("バランスは二の次で作っています");
		System.out.println("スキルはほぼ実装済みですが、一部状態異常や特殊スキル部分は未実装です。");
		System.out.println("問題無ければエンターを押してください");
		any=new java.util.Scanner(System.in).nextLine();
		System.out.println("キャラクター紹介");
		System.out.println("幸一＝主人公。攻撃系充実");
		System.out.println("愛莉紗＝主人公のメイド。攻防万能");
		System.out.println("シロ＝主人公の飼い猫。支援充実");
		System.out.println("龍之介＝主人公の兄。装甲破壊系充実");
		System.out.println("クイン・シー＝謎の狙撃手。");
		System.out.println("黒騎士＝敵。ケイオスという人物と共闘");
		System.out.println("問題無ければエンターを押してください");
		any=new java.util.Scanner(System.in).nextLine();
		System.out.println("ラストダンジョン通過中・・・");
		Thread.sleep(5000);
		System.out.println("幸一、愛莉紗、シロ、龍之介はとうとう宿敵・黒騎士のもとへ辿り着く。");
		Thread.sleep(3000);
		System.out.println("黒騎士『さあオーブよ、我が身を記憶せよ！』");
		Thread.sleep(3000);
	}

	/**
	 *
	 */
	//終わりのテキスト
	public void lastScript() {
		System.out.println("という訳で終了です。もっと実装したかったのがいくつかあり、");
		System.out.println("①専用ゲージつけて必殺技②状態異常の本実装③仲間キャラのガチャ（30キャラくらい作製）");
		System.out.println("④キャラクターごとのリンク状態（協力技）⑤スキルとは別枠で魔法の実装etc...");
		System.out.println("拘り出したらキリがないので、この辺りで妥協しました。");

	}

}
