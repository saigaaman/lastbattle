package lastBattle;

import java.util.List;

/**
 * @author 200217AM
 *
 */
public class Guard extends Enemy{//黒騎士のしもべクラス
	private int attackFrequency=0;//攻撃回数
	private int eSelectNum=0;//スキル番号
	private int target=0;//ターゲット
	private int mag=0;//黒騎士の攻撃倍率
	private int random=0;//乱数
	private int damage=0;//ダメージ
	private int critical=0;
	private int Avoidance=0;

	/* (非 Javadoc)
	 * @see lastBattle.Enemy#eSelect(java.util.List, java.util.List)
	 */
	//行動管理
	public void eSelect(List<Player> player, List<Enemy> enemy) throws InterruptedException {//ブラックナイトの攻撃分岐


		eSelectNum=new java.util.Random().nextInt(2)+1;//2通りの攻撃パターン

		switch(eSelectNum) {
			case 1://通常攻撃
				mag=80;
				target=new java.util.Random().nextInt(player.size());
					Avoidance = new java.util.Random().nextInt(100)+1;//回避の可否
					random=new java.util.Random().nextInt(31)+85;//ランダムで85～115を出す
					damage=(int)((this.getAttack()*5)-(player.get(target).getDefense()*2.5)
							*0.5);
					damage=(int)(damage*mag/100)*random/100;//ダメージをさらにスキル倍率やランダム倍率
					critical=new java.util.Random().nextInt(101);//0～100でランダム抽選
					System.out.println(this.getName()+"の通常攻撃！");
					if(this.getCri()>=critical) {//もしキャラのクリティカルにマッチしていたら
						damage=damage*2;//ダメージ２倍
						System.out.print("クリティカル攻撃！");
						Thread.sleep(1500);
					}
					if(player.get(target).getCounter()>0) {//もしカウンター状態なら
						player.get(target).goCounter();
						this.setHp(this.getHp() - damage);//ターゲットのHPを減らす。
						System.out.println(this.getName()+"に"+damage+"のダメージ！");
						Thread.sleep(1500);
						player.get(target).setCounter(player.get(target).getCounter()-1);//カウンターを減らす
					}else if(player.get(target).getAvoid()>=Avoidance) {//もしクリティカルじゃなければ回避可能
						player.get(target).toAvoidance();
						System.out.println(player.get(target).getName()+"は攻撃を回避した！");
						Thread.sleep(1500);
					}else {//クリティカルでもなく回避も出来ない場合
						damage=damage/10;//強すぎるので調整1/10
						player.get(target).setHp(player.get(target).getHp() - damage);//ターゲットのHPを減らす。
						System.out.println(player.get(target).getName()+"に"+damage+"のダメージ！");
						Thread.sleep(1500);
						if(player.get(target).getHp()<=0){
							player.get(target).setHp(0);
							System.out.println(player.get(target).getName()+"は戦闘不能になった！");
							Thread.sleep(1500);
						}
					}
				break;
			case 2://黒騎士のHPを20000回復
				enemy.get(0).setHp(enemy.get(0).getHp()+20000);
				System.out.println(this.getName()+"は黒騎士のHPを20000回復した！");
				break;
			default:
				break;
		}
	}

	/**
	 * @param name
	 */
	//コンストラクタ
	public Guard(String name) {//黒騎士を守るガードシステム
		super("ミラージュ・ナイト");
		this.seteNum(1);
		this.setName("ミラージュ・ナイト");
		this.setLevel(87);
		this.setMaxHp(50000);
		this.setMaxSp(9800);
		this.setHp(getMaxHp());
		this.setSp(getMaxSp());
		this.setAttack(1550);
		this.setMaAttack(1200);
		this.setDefense(1100);
		this.setMaDefense(1050);
		this.setCri(10);
		this.setAvoid(0);
		this.setSpeed(40);//最大100
		this.setTurn(50);
		this.setMaxArmourGuard(250);
		this.setArmourGuard(getMaxArmourGuard());
		this.setAmmo(6);//弾薬
		//ここから特殊状態
		this.setLimitBreak(0);//100でリミットブレイク発動、特定行動で消費
		this.setExtension(false);//高揚状態。キャラごとの条件で発動、ステータス大幅アップ
		//ここから状態異常
		this.setFireDamage(false);
		this.setIceBind(false);
		this.setStan(false);
		this.setSilent(false);
		//黒騎士作製ここまで

	}
}



