package lastBattle;

import java.util.ArrayList;
import java.util.List;

public class BattleStart {//バトルの進行を管理する中心となるクラス
	private boolean battleFlg;//バトルフラグ　falseでバトル終了
	private boolean battleWin;//勝利フラグ　trueで勝利
	private boolean playerSelect;
	private List<Player> player=new ArrayList<>();//味方参戦しているか否かをリスト配列格納
	private List<Player> breakPlayer=new ArrayList<>();
	private List<Enemy>enemy=new ArrayList<>();//敵キャラをリスト配列格納
	private int select=0;//行動を決める
	private int target=0;//対象を指定する為の変数
	private int targetType=0;//ターゲットのおおまかな分類
	private int targetRange=0;//ターゲットの範囲
	private int damageAttribute=0;
	private int damage=0;//ダメージ（敵味方共用）
	private int damageCount=0;
	private int critical=0;//クリティカル発動有無に使う
	private int supportType=0;
	private int supportNum=0;//サポート数値
	private int supportPoint[]=new int[8];
		//{HP,sp,attack,maAttack,def,mDef,cri,avoid,speed}
	private double skillBreak=0;//スキルにかかるアーマー破壊倍率
	private int armourBreak=0;//最終的なアーマーブレイク値
	private int random=0;//ダメージにかけるランダム変数
	private int skillType=0;
	private int num=0;//ダメージ以外の数値に
	private int frequency=0;//回数（連続攻撃などに共用）
	private double mag=0;//数値の倍率（共用）
	private int skillTurn;//スキルの持続ターン
	private int[] abnormalState=new int[5];//状態異常の数値。配列で受け渡す　使う？
	private int pSkillNum=0;
	private int battle1=0;//200でバトル1中、オーブに気づき、破壊する人を決めるまで
	private boolean battle1Flg=false;//最初、オーブを破壊するまでがtrue
	private int battle1Break=0;//400溜まれば専用セリフとともにオーブ破壊、battle1Flg消滅
	private boolean battle2Flg=false;//オーブ破壊からクイン・シーが配置につくまで
	private int battle2=0;//200クイン・シーの途中地点通過400で射撃地点到達
	private boolean battle3Flg=false;//最終決戦へ
	private int queencTurn=0;


	/**
	 * @throws InterruptedException
	 */
	/**
	 * @throws InterruptedException
	 */
	public void goBattle() throws InterruptedException{//バトルの進行を管理するメインメソッド
		//最初にバトルフラグをオンにし、敵味方を配列にそれぞれ格納
		storyScript story=new storyScript();
		battleFlg=true;//バトルフラグオン
		//player[1]=などでキャラ選択メソッドから選択させる？
		Koichi koichi =new Koichi("幸一");
		Arisa arisa =new Arisa("愛莉紗");
		Siro siro =new Siro("シロ");
		Ryunosuke ryunosuke=new Ryunosuke("龍之介");
		BlackKnight blackKnight=new BlackKnight("黒騎士");
		QueenC queenc=new QueenC();
		player.add(koichi);//幸一をプレイヤー0番に格納
		player.add(arisa);
		player.add(siro);
		player.add(ryunosuke);
		enemy.add(blackKnight);//黒騎士を敵0番に格納
		story.firstScript();
		blackKnight.callAvatar(player, enemy);
		blackKnight.callAvatar(player, enemy);
		System.out.println("黒騎士『フハハハハ！このオーブさえあれば、我が兵は無限に生み出せるのだ！』");
		Thread.sleep(2000);

		while(battleFlg){//バトルフラグ。全滅か勝利まで繰り返す
			if(battleFlg==true) {
				enemyWait();//敵のウェイトがたまって居たら行動を消化
			}
			if(battleFlg==true) {
				playerWait();//プレイヤーのウェイトがたまっていたら行動を消化
			}
			//ターン回ってきたところで状態異常管理メソッド（未実装）
			timeDigestion(story,player,breakPlayer,queenc);//敵味方時間経過メソッド

		}//ここまで戦闘繰り返し
		if(battleWin==true) {
			System.out.println("黒騎士『おのれえええええ！オーブよ、召喚はどうした！？うおおおおおおっ！』");
			Thread.sleep(5000);
			System.out.println("黒騎士は砕けて消え失せた。");
			Thread.sleep(3000);
			System.out.println("決戦に勝利した！");
			Thread.sleep(5000);
			story.lastScript();
		}else {
			System.out.println("全滅した・・・。");
		}


		//【実装状況】（バグ見つかれば修正）
		//・順番を回すメソッド→ステータスを表示するメソッド(各キャラクラス)→分岐でスキル等のメソッドで情報を呼び出し、ダメージ反映
		//・アーマーブレイク状態の実装(敵のみ。おおむね実装済み)。ブレイク中は減算無し

		//【未実装　必須部分】
		//・スキルやステータス文字列の書式設定→StringBuilderを使用（セリフ部分）
		//⑧消費SPを反映させる←TPにして攻撃したりダメージ受けると回復方式がいい？

		//【未実装　後回し部分】
		//・特定スキルに使うSPや道具が無い場合、メッセージが表示され同カテゴリ内選択し直しになる
		//・ステータス表記をもっと軽いものにする
		//・スレッドスリープで表記時間調整
		//・キャラクター追加予定
		//・二戦目はキャラクター選択式にする（多数出演）
		//while(getSize())で４人まで選択するようにするclearでやり直し繰り返し
		//・時間メソッドでスレッドを走らせ、制限時間を設定する
		//制限時間はTimeAPIで作成、０になるとゲームオーバーとなるようにする
		//条件達成で0になると、ゲームオーバー阻止
		//制限時間はnowでリアルタイムを反映するようにする
		//制限時間はキリのいい時間でシスアウト、残り１分からカウントダウンが始まる
		//・金剛実装　booleanで
		//【没アイディア】
		//スキルをArrayListで一括管理？←やめたほうがいい、余計ややこしい
		//拡張for文に置き換える←別に要らないのでは？
	}


	/**
	 * @param story
	 * @param player
	 * @param breakPlayer
	 * @param queenc
	 * @throws InterruptedException
	 */
	//バトル進行を管理するメソッド
	public void timeDigestion(storyScript story,List<Player> player, List<Player> breakPlayer,QueenC queenc) throws InterruptedException {
		for(int i=0;i<enemy.size(); i++) {
			if(enemy.get(i).getHp()>0) {//死んでいなければ
				enemy.get(i).setTurn(enemy.get(i).getTurn() + (enemy.get(i).getSpeed()/10));
			}
		}
		//参戦しているキャラのプレイヤーウェイト進行
		for(int i=0;i<player.size(); i++) {
			if(player.get(i).getHp()>0) {//死んでいなければ
				player.get(i).setTurn(player.get(i).getTurn() + (player.get(i).getSpeed()/10));
			}
		}
		battlePhase(story,queenc);//さらに、バトルフェーズを管理へ
	}

	/**
	 * @param story
	 * @param queenc
	 * @throws InterruptedException
	 */
	//バトルフェイズを管理するメソッド
	public void battlePhase(storyScript story, QueenC queenc) throws InterruptedException{

		if(battle1Flg==false){//まず最初に時間経過でbattle1をためる
			battle1+=10;
		}
		if(battle1==400&&battle1Flg==false){//400に達すると専用セリフ＋行くキャラを決める
			story.battle1Script(player,breakPlayer);
			battle1Flg=true;
		}
		if(battle1Flg==true){//キャラがオーブ破壊に動く
			battle1Break+=10;
		}
		if(battle1Flg==true&&battle1Break==400&&battle2Flg==false){//400に達し、破壊
			battle1Flg=false;
			enemy.get(0).setBattle1(true);//黒騎士、召喚不能
			story.battle2Script(player, breakPlayer);//オーブが破壊され、次の戦闘へ
			battle2Flg=true;//クイン・シーが狙撃地点へ走り出す
		}
		if(battle2Flg==true&&battle3Flg==false){
			battle1Break+=10;
			if(battle1Break==700){
				System.out.println("狙撃手クイン・シー、まもなく狙撃地点到達の模様！");
				Thread.sleep(5000);
			}else if(battle1Break==800){
				System.out.println("狙撃手クイン・シー、狙撃地点到達！");
				Thread.sleep(1500);
				System.out.println("強力な援護射撃が幸一達をサポート！");
				Thread.sleep(5000);
				System.out.println("クイン・シーの『特殊射撃』！");
				enemy.get(0).setAttack((int)(enemy.get(0).getAttack()/0.8));
				enemy.get(0).setMaAttack((int)(enemy.get(0).getMaAttack()/0.8));
				enemy.get(0).setDefense((int)(enemy.get(0).getDefense()/0.8));
				enemy.get(0).setMaDefense((int)(enemy.get(0).getMaDefense()/0.8));
				System.out.println("黒騎士の攻撃力・防御力が低下！");
				Thread.sleep(5000);
				battle2Flg=false;
				battle3Flg=true;
				enemy.get(0).setBattle1(false);//
				enemy.get(0).setBattle2(true);//クイン・シー参戦状態へ
			}
		}else if(battle3Flg==true){
			queencTurn+=10;
			if(queencTurn>=100){
				queenc.goSkill(enemy,player);
				queencTurn-=100;
			}
		}
	}

	/**
	 * @throws InterruptedException
	 */
	//ウェイトが溜まった敵の行動を管理するメソッド
	public void enemyWait() throws InterruptedException {//敵の攻撃処理
		for(int i=0; i<enemy.size(); i++) {//順番にキャラクターのウェイトが100溜まってるか見ていく

			if(enemy.get(i).getTurn()>=100) {
				enemy.get(i).eSelect(player,enemy);//処理させる為、インスタンスを渡す
				enemy.get(i).setTurn(enemy.get(i).getTurn()-100);
			}

		}

	}


	/**
	 * @param enemy
	 * @throws InterruptedException
	 */
	//敵のステータスを表示するメソッド
	public void enemyStatus(List<Enemy> enemy) throws InterruptedException{
		final String FORMAT="%-7s　HP%-3d％　アーマー%-3d％　ウェイト%-3d％";
		System.out.println("【Enemy】");
		for(int i=0; i<enemy.size();i++) {
			String s=String.format
				(FORMAT,enemy.get(i).getName(),(enemy.get(i).getHp()*100/enemy.get(i).getMaxHp()*100)/100,
						enemy.get(i).getArmourGuard()*100/enemy.get(i).getMaxArmourGuard()*100/100,enemy.get(i).getTurn());
			System.out.println(s);
		}
		System.out.println();
		Thread.sleep(1000);
	}





	/**
	 * @param player
	 * @throws InterruptedException
	 */
	//味方キャラのステータスを表示するメソッド
	public void playerStatus(List<Player> player) throws InterruptedException {//プレイヤーのHPやSPを表記するメソッド
		///////////////////////プレイヤーの数だけプレイヤーの状態をメッセージに出す
		//continueかbreakでいける？
		Thread.sleep(2000);
		final String FORMAT="%-7s　HP%-6d　SP%-6d　ウェイト%-3d％";
		System.out.println("【PLayer】");

		for(int i=0; i<player.size();i++) {
			String s=String.format
					(FORMAT,player.get(i).getName(),player.get(i).getHp(),player.get(i).getSp(),player.get(i).getTurn());
			System.out.println(s);
		}


	}


	/**
	 * @throws InterruptedException
	 */
	//ウェイトがたまった味方の行動を管理するメソッド
	public void playerWait() throws InterruptedException {//プレイヤーの行動全般を管理するメソッド バトルのメイン計算関係は全部ここで処理する
		Skill skill=new Skill(select);//【何度もnewして問題無いか？先生に聞く】
		////////////////////////プレイヤーのウェイトが100溜まったら行動
		for(int i=0; i<player.size(); i++) {//順番にキャラクターのウェイトが100溜まってるか見ていく
			if(player.get(i).getTurn()>=100) {
				playerSelect=false;

				while(playerSelect==false) {//プレイヤーの選択肢が正しく選ばれるまでループ
					target=0;//ターゲットを最初の状態に
					playerStatus(player);//プレイヤーのHPやSP表記するメソッド
					enemyStatus(enemy);
					select=player.get(i).goSelect(select);//プレイヤーの行動を分類
					if(select==1){//もし通常攻撃が選択されたら
						normaleAttack(i);//通常攻撃メソッドへ
					}else if(select==2) {//もしスキルが選択されているならスキル選択肢へ
						pSkillNum=player.get(i).goSkill();//スキル番号を取得
						skillClassification(pSkillNum,skill);//スキル変数に値を入力
						switch(skillType){//スキルタイプにより発動方法分岐
							case 1://物理攻撃スキル(単体・全体・ランダム)
								armourBreak=(int)Math.ceil(player.get(i).getDefBreak()*skillBreak);//通常攻撃のアーマーブレイク値
								System.out.println("アーマーブレイク値は"+armourBreak);
								Thread.sleep(1500);
								damageCount=0;
								if(targetRange==1) {//もしターゲットが敵単体選択式なら
									singleAttack(i,skill);//シングル攻撃へ。ターゲッティングあるのでスキル引数
								}else if(targetRange==2){//全体攻撃
									skill.skillScript(pSkillNum);//スキル発動前のセリフを表記
									allAttack(i);//全体攻撃メソッドへ。ターゲッティングなし

								}else if(targetRange==3){//敵全体を対象としたランダム攻撃
									skill.skillScript(pSkillNum);
									randomAllAttack(i);
								}
								break;
							case 2://魔法攻撃スキル
								armourBreak=(int)Math.ceil(player.get(i).getDefBreak()*skillBreak);//通常攻撃のアーマーブレイク値
								System.out.println("アーマーブレイク値は"+armourBreak);
								Thread.sleep(1500);
								damageCount=0;
								if(targetRange==1) {//もしターゲットが敵単体選択式なら
										singleMaAttack(i,skill);
								}else if(targetRange==2){//全体攻撃
									skill.skillScript(pSkillNum);//スキル発動前のセリフを表記
									allMaAttack(i);
								}
								break;
							case 3://回復スキル
								//単体
								if(targetRange==1) {//もしターゲットが単体選択式なら
									singleRecovery(i,skill);
								//全体
								}else if(targetRange==2){//全体回復
									allRecovery(i,skill);
								}
								break;
							case 4://補助スキル

								break;
							case 5://特殊スキル
								if(pSkillNum==12) {//愛莉紗の羅刹なら
									skill.skillScript(pSkillNum);
									player.get(i).setCounter(2);//カウンター状態発動（2回）
								}
								break;
							default:
								break;
						}
						if(pSkillNum!=10&&target!=10) {//もし10（戻る）を選択していない場合だけ
								playerSelect=true;//ループから脱
						}
						//////////////////////スキルここまで////////////////////////////////////////////
					}else {//それ以外ならもう一度繰り返す
							System.out.println("ちゃんと選べ。防御・道具・ショットは未実装だ！");
					}
				}//プレイヤーの選択肢が正しく選ばれ、効果が反映しているならループ脱出
					System.out.println();//改行
					player.get(i).setTurn(player.get(i).getTurn()-100);//行動を終えたキャラのターンを-100にする
					//100にする理由としては100以上になった場合、無駄になるから。
			}
			if(enemy.size()==0) {//もし敵キャラ全てを全滅させたら
				battleWin=true;
				battleFlg=false;
				break;//ブレイクだ！
			}
		}//for文はここまで。１キャラ分の行動の終点

	}//ウェイト溜まっているプレイヤーの行動を全て消化でメソッド終了





	//スキルの変数変換メソッド。スキル数値をそれぞれ値を代入
	/**
	 * @param pSkillNum
	 * @param skill
	 */
	//スキルナンバーからスキルのあらゆる数値を取得するメソッド
	public void skillClassification(int pSkillNum, Skill skill) {
		skillType=skill.skillType(pSkillNum);//スキルのタイプを取得
		targetRange=skill.skillRange(pSkillNum);//ターゲットの範囲（選択式・all・選択できない）
		mag=skill.skillMag(pSkillNum);//スキルの攻撃倍率を取得
		supportType=skill.supportType(pSkillNum);//サポートタイプを選択
		supportPoint=skill.supportPoint(pSkillNum);//サポート数値を要求
		//{HP,sp,attack,maAttack,def,mDef,cri,avoid,speed}
		skillBreak=skill.skillBreak(pSkillNum);//スキルの装甲破壊倍率
		frequency=skill.skillfrequency(pSkillNum);//スキルの回数を取得
		skillTurn=skill.skillTurn(pSkillNum);//スキルの有効ターンを取得

	}

	/**
	 * @param i
	 * @param skill
	 * @throws InterruptedException
	 */
	//単体物理攻撃メソッド
	public void singleAttack(int i,Skill skill) throws InterruptedException {
		target=player.get(i).goAttack(enemy);//ターゲット選択メソッドへ
		if(target==10) {//ターゲット選択でもし10(やっぱやめる)が選ばれたら
			System.out.println("最初の選択肢に戻ります。");//何もしない
		}else {//それ以外なら攻撃を当てはめる。倍率は小数点切り上げ
			skill.skillScript(pSkillNum);//スキル発動前のセリフを表記
			armorBreakEndCheck();//アーマーブレイクチェック
			for(int a=0;a<frequency;a++) {//スキルの攻撃回数分、ダメージ繰り返し
					lastDamage(i);//最終的なダメージを算出し、減算を表示する
					armorBreakStartCheck();
			}//攻撃回数による繰り返し終わり
			if(enemy.get(target).getHp()<=0) {//もし敵を倒していたら
				System.out.println(enemy.get(target).getName()+"を倒した！");
				enemy.remove(target);
			}
			if(frequency>=2) {
				System.out.println("合計"+frequency+"ヒット！"+damageCount+"ダメージ！");
				Thread.sleep(3000);
			}
		}//単体攻撃の処理終わり
	}


	/**
	 * @param i
	 * @throws InterruptedException
	 */
	//全体物理攻撃
	public void allAttack(int i) throws InterruptedException {
		for(target=0; target<enemy.size();target++){//全体攻撃用
			armorBreakEndCheck();
		}
		for(int a=0;a<frequency;a++) {//スキルの攻撃回数分、ダメージ繰り返し
			for(target=0; target<enemy.size();target++){//enemy(s)=敵の数だけ繰り返す
					lastDamage(i);
					armorBreakStartCheck();
			}
		}//攻撃回数による繰り返し終わり
		if(frequency>=2) {
			System.out.println("合計"+frequency+"ヒット！"+damageCount+"ダメージ！");
			Thread.sleep(3000);
		}
		for(target=0; target<enemy.size();target++){//enemy(s)=敵の数だけ繰り返す
			if(enemy.get(target).getHp()<=0) {//もし敵を倒していたら
				System.out.println(enemy.get(target).getName()+"を倒した！");
				enemy.remove(target);//消す
				Thread.sleep(3000);
				target--;//チェック繰り返し
			}
		}
		if(pSkillNum==11) {//もし愛莉紗の風刃なら
			for(target=0; target<enemy.size();target++){//enemy(s)=敵の数だけ繰り返す
				enemy.get(target).setTurn(enemy.get(target).getTurn()-20);
				System.out.println("さらに"+enemy.get(target).getName()+"に行動遅延を付与！");
				Thread.sleep(2000);
			}
		}
		pSkillNum=0;//全体攻撃の行動消費のため初期化
		target=0;//同上
	}

	/**
	 * @param i
	 * @throws InterruptedException
	 */
	//全体ランダム物理攻撃
	public void randomAllAttack(int i) throws InterruptedException {
		for(target=0; target<enemy.size();target++){//全体攻撃用
			armorBreakEndCheck();
		}
		for(int a=0;a<frequency;a++) {//スキルの攻撃回数分、ダメージ繰り返し
				if(enemy.size()==1){//敵が一人ならターゲットは0固定
					target=0;
				}else{//敵が二人以上ならランダム選択し、-1
					target=new java.util.Random().nextInt(enemy.size());//敵の数だけランダム要素
				}
				lastDamage(i);
				armorBreakStartCheck();
		}//攻撃回数による繰り返し終わり
		if(frequency>=2) {
		System.out.println("合計"+frequency+"ヒット！"+damageCount+"ダメージ！");
		Thread.sleep(3000);
		}
		for(target=0; target<enemy.size();target++){//enemy(s)=敵の数だけ繰り返す
			if(enemy.get(target).getHp()<=0) {//もし敵を倒していたら
				System.out.println(enemy.get(target).getName()+"を倒した！");
				enemy.remove(target);//消す
				Thread.sleep(3000);
				target--;//チェック繰り返し
			}
		}
		pSkillNum=0;//全体攻撃の行動消費のため初期化
		target=0;//同上
	}


	/**
	 * @param i
	 * @throws InterruptedException
	 */
	//全体魔法攻撃メソッド
	public void allMaAttack(int i) throws InterruptedException {
		for(target=0; target<enemy.size();target++){//敵全体にアーマーブレイク回復チェック
			armorBreakEndCheck();
		}
		for(int a=0;a<frequency;a++) {//スキルの攻撃回数分、ダメージ繰り返し
			for(target=0; target<enemy.size();target++){//enemy(s)=敵の数だけ繰り返す
				lastMaDamage(i);
				armorBreakStartCheck();
			}
		}//攻撃回数による繰り返し終わり
		for(target=0; target<enemy.size();target++){
			if(enemy.get(target).getHp()<=0) {//もし敵を倒していたら
				System.out.println(enemy.get(target).getName()+"を倒した！");
				Thread.sleep(3000);
				enemy.remove(target);
				target--;
			}
		}
		pSkillNum=0;//全体攻撃の行動消費のため初期化
		target=0;//同上
	}

	/**
	 * @param i
	 * @param skill
	 * @throws InterruptedException
	 */
	//単体魔法攻撃メソッド
	public void singleMaAttack(int i,Skill skill) throws InterruptedException {
		target=player.get(i).goAttack(enemy);//ターゲット選択メソッドへ
		if(target==10) {//ターゲット選択でもし10(やっぱやめる)が選ばれたら
			System.out.println("最初の選択肢に戻ります。");//何もしない
		}else {//それ以外なら攻撃を当てはめる。倍率は小数点切り上げ
			skill.skillScript(pSkillNum);//スキル発動前のセリフを表記
			armorBreakEndCheck();//アーマーブレイクチェック
			for(int a=0;a<frequency;a++) {//スキルの攻撃回数分、ダメージ繰り返し
					lastMaDamage(i);//最終的なダメージを算出し、減算を表示する
					armorBreakStartCheck();
			}//攻撃回数による繰り返し終わり
			if(enemy.get(target).getHp()<=0) {//もし敵を倒していたら
				System.out.println(enemy.get(target).getName()+"を倒した！");
				enemy.remove(target);
			}
			if(frequency>=2) {
				System.out.println("合計"+frequency+"ヒット！"+damageCount+"ダメージ！");
				Thread.sleep(3000);
			}
		}//単体攻撃の処理終わり
	}

	/**
	 * @param i
	 * @param skill
	 * @throws InterruptedException
	 */
	//単体回復メソッド
	private void singleRecovery(int i, Skill skill) throws InterruptedException {
		target=player.get(i).goSupport(player);//ターゲット選択メソッドへ
		if(target==10) {//ターゲット選択でもし10(やっぱやめる)が選ばれたら
			System.out.println("最初の選択肢に戻ります。");//何もしない
		}else {//それ以外なら回復を当てはめる
			//倍率は小数点切り上げ
			skill.skillScript(pSkillNum);//スキル発動前のセリフを表記
			if(player.get(target).getHp()<=0&&pSkillNum!=24) {//もしプレイヤーのHPがゼロ以下で
				//かつ蘇生技の【起こす】でなければ
				System.out.println(player.get(target).getName()+"は戦闘不能の為回復できない！");
				Thread.sleep(1500);
			}else{//それ以外なら回復
				supportNum=(int)(player.get(target).getMaxHp()*mag)/100;//maxhpに倍率かけて1/100
			//割合数値を割り出す
				player.get(target).setHp(player.get(target).getHp()+supportNum);
				if(player.get(target).getHp()>player.get(target).getMaxHp()) {
				//回復量が最大HPを上回った場合
				player.get(target).setHp(player.get(target).getMaxHp());
				//MAXHPにする
				}
			//味方のHPを回復
				System.out.println(player.get(target).getName()+"のHPが"+supportNum+"回復！");
				Thread.sleep(1500);
			}
		}//単体補助の処理終わり
	}

	/**
	 * @param i
	 * @param skill
	 * @throws InterruptedException
	 */
	//全体回復めそっど
	public void allRecovery(int i, Skill skill) throws InterruptedException {//全体回復めそっど
		skill.skillScript(pSkillNum);//スキル発動前のセリフを表記
		for(int r=0;r<player.size();r++) {
			if(player.get(r).getHp()<=0&&pSkillNum!=24) {//もしプレイヤーのHPがゼロ以下で
				//かつ蘇生技の【起こす】でなければ
				System.out.println(player.get(r).getName()+"は戦闘不能の為回復できない！");
				Thread.sleep(1500);
			}else{//それ以外なら回復
				supportNum=(int)(player.get(r).getMaxHp()*mag)/100;//maxhpに倍率かけて1/100
				//割合数値を割り出す
				player.get(r).setHp(player.get(r).getHp()+supportNum);
				if(player.get(r).getHp()>player.get(r).getMaxHp()) {
				//回復量が最大HPを上回った場合
					player.get(r).setHp(player.get(r).getMaxHp());
					//MAXHPにする
				}
				//味方のHPを回復
				System.out.println(player.get(r).getName()+"のHPが"+supportNum+"回復！");
				Thread.sleep(1500);
			}
			if(pSkillNum==13){//涼風なら
				System.out.println("さらに"+player.get(r).getName()+"行動速度を少し早めた！");
				player.get(r).setTurn(player.get(r).getTurn()+10);
			}
		}
	}



	/**
	 * @throws InterruptedException
	 */
	//アーマーブレイク回復有無チェックメソッド
	public void armorBreakEndCheck() throws InterruptedException {
		enemy.get(target).setArmorBreakTurn(enemy.get(target).getArmorBreakTurn()-1);
		//アーマーブレイクターンを-1
		if(enemy.get(target).getArmorBreakTurn()==0&&enemy.get(target).isArmorBreak()==true) {//もし装甲破壊ターン終わったら
			enemy.get(target).setArmourGuard(enemy.get(target).getMaxArmourGuard());
			enemy.get(target).setArmorBreak(false);//装甲もとに戻して状態も解除
			System.out.println(enemy.get(target).getName()+"がアーマーブレイク状態から回復！");
			Thread.sleep(1500);
		}

	}
	/**
	 * @param i
	 * @throws InterruptedException
	 */
	//最終的な魔法ダメージを出すメソッド
	public void lastMaDamage(int i) throws InterruptedException {
		damage=(int)((player.get(i).getMaAttack()*5)-(enemy.get(target).getMaDefense()*2.5)
				*0.5);
		if(enemy.get(target).isArmorBreak()==true) {//もし敵がアーマーブレイクしたら
			System.out.println(enemy.get(target).getName()+"はアーマーブレイク中！");
			Thread.sleep(1500);
			damage=(int)(player.get(i).getMaAttack()*5);//防御無視攻撃！
		}
		//プレイヤーの攻撃力に対し対象のディフェンスで減算
		//物理攻撃：{(攻撃側のattack × 5) - (対象のdef × 2.5)} × 0.5=基本物理ダメージ
		//魔法攻撃：{(攻撃側のmaattack × 5) - (対象のmdef × 2.5)} × 0.5＝基本魔法ダメージ
		random=new java.util.Random().nextInt(31)+85;//ランダムで85～115を出す
		damage=(int)(damage*mag/100)*random/100;//ダメージをさらにスキル倍率やランダム倍率
		critical=new java.util.Random().nextInt(101);//0～100でランダム抽選
		if(player.get(i).getCri()>=critical) {//もしキャラのクリティカル成功値にマッチしていたら
			damage=damage*2;//ダメージ２倍
			//クリティカル判定
			System.out.print("クリティカル攻撃！");
			Thread.sleep(1500);
		}
		enemy.get(target).setHp(enemy.get(target).getHp() - damage);//ターゲットのHPを減らす。
		enemy.get(target).setArmourGuard(enemy.get(target).getArmourGuard()-armourBreak);
		System.out.println(enemy.get(target).getName()+"に"+damage+"のダメージ！");
		Thread.sleep(1500);
		damageCount+=damage;

	}


	/**
	 * @param i
	 * @throws InterruptedException
	 */
	//最終的な物理ダメージ計算、減算、表記のメソッド
	public void lastDamage(int i) throws InterruptedException {
		damage=(int)((player.get(i).getAttack()*5)-(enemy.get(target).getDefense()*2.5)
				*0.5);
		if(pSkillNum==2||pSkillNum==32) {//もし煉獄か煉獄弾なら防御減算多め
			damage=(int)((player.get(i).getAttack()*5)-(enemy.get(target).getDefense()*3.5)
					*0.5);
		}
		if(enemy.get(target).isArmorBreak()==true) {//もし敵がアーマーブレイクしたら
			System.out.println(enemy.get(target).getName()+"はアーマーブレイク中！");
			Thread.sleep(1500);
			damage=(int)(player.get(i).getAttack()*5);//防御無視攻撃！
		}else if(pSkillNum==3){//もしくは幸一のスキル【絶影】なら
			damage=(int)(player.get(i).getAttack()*5);//デフォルトで防御無視攻撃！
		}

		//プレイヤーの攻撃力に対し対象のディフェンスで減算
		//物理攻撃：{(攻撃側のattack × 5) - (対象のdef × 2.5)} × 0.5=基本物理ダメージ
		//魔法攻撃：{(攻撃側のmaattack × 5) - (対象のmdef × 2.5)} × 0.5＝基本魔法ダメージ
		random=new java.util.Random().nextInt(31)+85;//ランダムで85～115を出す
		damage=(int)(damage*mag/100)*random/100;//ダメージをさらにスキル倍率やランダム倍率
		critical=new java.util.Random().nextInt(101);//0～100でランダム抽選
		if(player.get(i).getCri()>=critical||pSkillNum==14) {//もしキャラのクリティカルにマッチしていたら
			//もしくは確定クリティカルなら
			damage=damage*2;//ダメージ２倍
			System.out.print("クリティカル攻撃！");
			Thread.sleep(1500);
		}
			enemy.get(target).setHp(enemy.get(target).getHp() - damage);//ターゲットのHPを減らす。
			enemy.get(target).setArmourGuard(enemy.get(target).getArmourGuard()-armourBreak);
			System.out.println(enemy.get(target).getName()+"に"+damage+"のダメージ！");
			Thread.sleep(1500);
			damageCount+=damage;


	}

	/**
	 * @throws InterruptedException
	 */
	//敵がアーマーブレイクしたか管理するメソッド
	public void armorBreakStartCheck() throws InterruptedException {
		if(enemy.get(target).getArmourGuard()<=0&&enemy.get(target).isArmorBreak()==false){//敵アーマーがゼロになったら
			enemy.get(target).setArmorBreak(true);//アーマーブレイク状態
			System.out.println(enemy.get(target).getName()+"、アーマーブレイク！");
			Thread.sleep(1500);
			enemy.get(target).setArmorBreakTurn(5);//ターンを5に設定
				//敵にターンが来るor攻撃を与える都度に-1

		}

	}


	/**
	 * @param i
	 * @throws InterruptedException
	 */
	//通常攻撃メソッド
	public void normaleAttack(int i) throws InterruptedException {
		mag=100;
		armourBreak=(int)Math.ceil(player.get(i).getDefBreak()*1.0);//通常攻撃のアーマーブレイク値
		target=player.get(i).goAttack(enemy);//通常攻撃メソッドへ
		if(target>=0 && target<=enemy.size()) {//ターゲットが0以上かつ、敵の数以下
			armorBreakEndCheck();//アーマーブレイクが終了したかどうか
			damageAttribute=Math.max(player.get(i).getAttack(),player.get(i).getMaAttack());
			System.out.println("攻撃基本値は"+damageAttribute);
			Thread.sleep(1500);
			player.get(i).attackScript();
			//物理攻撃か魔法攻撃、大きい数値をとる
				lastDamage(i);
				armorBreakStartCheck();
				if(enemy.get(target).getHp()<=0) {//もし敵を倒していたら
					System.out.println(enemy.get(target).getName()+"を倒した！");
					enemy.remove(target);
				}
			playerSelect=true;//行動が正しく選択された

		}else if(target==10) {
			System.out.println("最初の選択肢に戻ります。");
		}

	}





	/*	画面推移→序盤メッセージからの戦闘開始画面
	 * →初期キャラクターは幸一のみ（毎ターン参戦フラグで仲間が参戦していく）
	 *
	 * 戦闘推移
	 * ターン制ではなくスピード値ごとに行動が回ってくる（スピードにより溜まる数字を繰り返し分で増やし、一定数値
	 * 到達でコマンド可能、コマンド実行時に数字を０へ）、
	 *
	 * ターン来る→行動決める→即発動→ターン待ち（軌跡シリーズ式）
	 * while内には
	 * ①戦闘継続出来るか（判定はHPが全員0になるか）と
	 * ②スピードにより待機数値を上げ、最大になればコマンド入力待機になる処理→完了
	 * ③コマンド選択を受け入れ、各コマンドへのリンク、コマンドによる状態変化有無
	 * 主人公だけオーダー発動可能、オーダーへリンク
	 * ④同時に仲間参戦値を設定、一定の時間経過で仲間参戦有無判定
	 * ⑤状態変化や状態異常も数値で管理、数値が上がり続け、一定値到達で切れるようにする
	 * ⑥敵も同様。行動はランダム値。仲間参戦ごとに強化？
	 *
	 *
	 *仲間ステータス
	 * 【0】幸一　攻撃力Ｂ　破壊B　防御A　速度Ｃ　支援Ｃ　総合力B
	 *  主人公。装甲破壊後の煉獄が高威力。
	 *
	 * 【1】愛莉紗　攻撃力Ａ　破壊B　防御Ｂ　速度Ｓ　支援Ａ　総合力A
	 * 最強万能メイド。全てに優れる。
	 *
	 * 【2】シロ　攻撃力Ｃ　破壊C　防御Ｃ　速度Ａ　支援Ｓ　総合力B
	 * 白毛の猫。支援が得意。
	 *
	 * 【3】龍之介　攻撃力Ａ　破壊S　防御Ａ　速度Ｃ　支援Ｄ　総合力A
	 * 攻防に優れ、装甲破壊能力に長けている。
	 *
	 * 【4】美麗　攻撃力Ｓ　破壊B　防御C　速度A　支援Ｄ　総合力B
	 * スタンの蓄積数値が高いアタッカー。対多数に強い
	 *
	 * 【5】ジャック　攻撃力A　破壊A　防御A　速度A　支援D　総合力A
	 * 全ての敵に対応したスーパーアタッカー。
	 *
	 *
	 *
	 * 【?】クイン・シー
	 * 参戦すると様々なスキルでサポートしてくれる。
	 * 参戦が特殊枠で、射撃地点に到達すれば参戦扱いになる。
	 *
	 *
	 *
	 *
	 */










}
