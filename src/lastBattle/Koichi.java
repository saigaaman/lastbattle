package lastBattle;

import java.util.List;

public class Koichi extends Player {// 主人公の幸一を管理するメソッド。
	// プレイヤーの基本特徴を引き継いでいる

	private String name = "幸一";
	private int select = 0;
	private int skillNum;
	private boolean skillStandby = false;

	/*
	 * (非 Javadoc)
	 *
	 * @see lastBattle.Player#goSelect(int)
	 */
	// 行動選択メソッド
	public int goSelect(int select) throws InterruptedException {
		System.out.println(getName() + "『俺の番だな・・・』");
		System.out.println(getName() + "の行動を選択してください。");
		Thread.sleep(1000);
		System.out.println("【1】通常攻撃【2】スキル【3】射撃【4】防御【5】アイテム");
		select = new java.util.Scanner(System.in).nextInt();
		//// Sp残量によってはスキルを選べないようにすると良いんでないか？
		//// getSpで分岐、スキルを選ぶと「どのスキルもSPが足りない！」と。
		return select;
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see lastBattle.Player#goAttack(java.util.List)
	 */
	//// 通常攻撃および攻撃対象指定スキルで呼ばれるメソッド。
	public int goAttack(List<Enemy> enemy) throws InterruptedException {
		boolean selectAttack = false;
		int target = 0;
		while (selectAttack == false) {
			for (int i = 0; i < enemy.size(); i++) {// 敵の数だけ名前とともに並べる
				System.out.println("【" + i + "】" + enemy.get(i).getName());
				Thread.sleep(1000);
			}
			System.out.println("対象を選んで下さい");
			System.out.println("もし、最初の選択肢に戻る場合は【10】を選択して下さい。");
			target = new java.util.Scanner(System.in).nextInt();

			if (target == 10) {// 最初の選択肢に戻りたい場合
				selectAttack = true;
			} else if (target > enemy.size()) {// もし敵数より多い数字を入力していたら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			} else if (target == 0 || target == 1 || target == 2 || target == 3) {// 敵の数が一致したら番号を確認
				selectAttack = true;// 敵選択完了
			} else {// それ以外の文字や、入力何もしなかったら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			}
		} //
		return target;// 誰を攻撃するか返す
	}

	/* (非 Javadoc)
	 * @see lastBattle.Player#goSupport(java.util.List)
	 */
	//指定型サポートスキルで呼ばれるメソッド。
	public int goSupport(List<Player> player) throws InterruptedException {
		boolean selectSupport = false;
		int target = 0;
		while (selectSupport == false) {
			for (int i = 0; i < player.size(); i++) {// 敵の数だけ名前とともに並べる
				System.out.println("【" + i + "】" + player.get(i).getName());
				Thread.sleep(1000);
			}
			System.out.println("対象を選んで下さい");
			System.out.println("もし、最初の選択肢に戻る場合は【10】を選択して下さい。");
			target = new java.util.Scanner(System.in).nextInt();
			if (target == 10) {// 最初の選択肢に戻りたい場合
				selectSupport = true;
			}
			else if (target > player.size()) {// もし敵数より多い数字を入力していたら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			} else if (target == 0 || target == 1 || target == 2 || target == 3) {// 敵の数が一致したら番号を確認
				selectSupport = true;// 敵選択完了
			} else {// それ以外の文字や、入力何もしなかったら
				System.out.println("正しい対象を選択してください");
				Thread.sleep(1000);
			}
		} //
		return target;
	}

	/* (非 Javadoc)
	 * @see lastBattle.Player#goSkill()
	 */
	//スキル選択めそっど
	public int goSkill() throws InterruptedException {
		skillStandby = false;// バトルリザルトフラグをリセット
		select = 0;// セレクトをリセット
		skillNum = 0;// スキル番号をリセット

		while (skillStandby == false) {// スキルを決めてから、スキルの効果を実行するまでループ
			System.out.println(getName() + "『どの技を使うべきだろうか……』");
			Thread.sleep(1000);
			//////// 幸一スキル
			System.out.println("1『雑賀の霊弾』味方全員のattack・avoid ３ターン20%上昇（未実装）");
			Thread.sleep(500);
			System.out.println("2『煉獄』　　　威力SS 敵一体に多段攻撃 敵物理防御が低ければダメージ増加");
			Thread.sleep(500);
			System.out.println("3『絶影』　　　威力B 敵全体に防御無視攻撃");
			Thread.sleep(500);
			System.out.println("4『螺旋』　　　威力A 敵一体に物理攻撃+スタン確率20%（スタンは未実装）");
			Thread.sleep(500);
			System.out.println("5『マキシマムドライブ　ジョーカーエクストリーム』　チート技。正常なプレイでは選択しないで下さい。");
			System.out.println("上記何れかの番号を入力してください。");
			Thread.sleep(500);
			System.out.println("もし、最初の選択肢に戻る場合は【10】を選択して下さい。");
			select = new java.util.Scanner(System.in).nextInt();
			if (select == 1) {// もし雑賀の霊弾を選んでいたら
				skillNum = 1;
				skillStandby = true;
			} else if (select == 2) {// もし煉獄を選んでいたら
				skillNum = 2;
				skillStandby = true;
			} else if (select == 3) {// もし絶影を選んでいたら
				skillNum = 3;
				skillStandby = true;
			} else if (select == 4) {// もし螺旋を選んでいたら
				skillNum = 4;
				skillStandby = true;
			} else if (select == 5) {// もし５を選んでいたら
				skillNum = 5;
				skillStandby = true;
			} else if (select == 10) {// もし戻るを選んでいたら
				skillNum = 10;// 10をリターンする
				System.out.println(getName() + "『くっ……もう一度考え直すか……！』");
				Thread.sleep(1000);
				skillStandby = true;
			} else {
				System.out.println(getName() + "『違う、そうじゃないだろう！』");
				Thread.sleep(1000);
			}
		}
		if (select != 10) {
			System.out.println(getName() + "『よし、これで行こう。』");
			Thread.sleep(1000);
		}
		return skillNum;
	}

	/* (非 Javadoc)
	 * @see lastBattle.Player#toAvoidance()
	 */
	//回避時セリフ
	public void toAvoidance() {
		System.out.println(this.name + "『おっと……危なかったな……。』");
	}

	/* (非 Javadoc)
	 * @see lastBattle.Player#attackScript()
	 */
	//通常攻撃時セリフ
	public void attackScript() throws InterruptedException {
		System.out.println(this.name + "『はあっ！食らえっ！』");
		Thread.sleep(1000);
		System.out.println(this.name + "の通常攻撃！");
		Thread.sleep(1000);
	}

	public void goShot() {// 後回しで
		// 残弾数
		// anyで連発
		// stopで停止
		// stop後行動を戻す
	}

	public void goGuird() {// 後回し
	}

	public void goItem() {// 後回し
	}

	/**
	 * @param name
	 */
	public Koichi(String name) {// コンストラクタ
		super("幸一");
		this.setcNum(0);// キャラクターナンバー
		this.name = "幸一";
		this.setLevel(68);
		this.setMaxHp(7100);
		this.setMaxSp(2400);
		this.setHp(getMaxHp());
		this.setSp(getMaxSp());
		this.setAttack(1150);
		this.setMaAttack(875);
		this.setDefense(1030);
		this.setMaDefense(795);
		this.setCri(10);// クリティカル率
		this.setAvoid(10);// 回避率（％）
		this.setSpeed(60);// 最大100
		this.setTurn(0);
		this.setDefBreak(20);// 最大100
		this.setAmmo(6);// 弾薬
		// ここから特殊状態
		this.setLimitBreak(0);// 100でリミットブレイク発動、特定行動で消費
		this.setExtension(false);// 高揚状態。キャラごとの条件で発動、ステータス大幅アップ

		// ここから状態異常
		this.setFireDamage(false);
		this.setIceBind(false);
		this.setStan(false);
		this.setSilent(false);
		// 幸一作製ここまで

	}

}
