package lastBattle;

import java.util.List;

public class Enemy {//敵メソッド。全ての敵キャラの基本
	private int eNum;//敵の番号
	private String name;//名前
	private int level;//レベル
	private int maxHp=0;
	private int maxSp=0;
	private int hp=getMaxHp();//体力。能力初期値は個々数値＋レベル分
	private int sp=getMaxSp();//スキルポイント
	private int attack;//物理攻撃力
	private int maAttack;//特殊攻撃力
	private int defense;//物理防御力
	private int maDefense;//特殊防御力
	private int cri;//クリティカル率
	private int avoid;//回避
	private int speed;//スピードはキャラレベルにより変化しない固有数値
	private int turn;//speedに応じて増加。行動で0へ
	private int maxArmourGuard;
	private int armourGuard;//アーマーガード
	private int ammo;//弾薬。戦闘の際、補給部隊が補充してくれる。

	//状態変更
	private int limitBreak;
	private boolean extension;//士気高揚状態
	private boolean armorBreak;//アーマーブレイク状態
	private int armorBreakTurn=0;

	//状態異常
	private boolean fireDamage;//炎上状態
	private boolean iceBind;//凍結状態
	private boolean stan;//スタン状態
	private boolean silent;//沈黙状態
	private boolean bind;//拘束状態

	public Enemy(String name) {//敵のコンストラクタ
		this.setName(name);
		this.setHp(100);

	}

	public int geteNum() {
		return this.eNum;
	}

	public void seteNum(int eNum) {
		this.eNum = eNum;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLevel() {
		return this.level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getHp() {
		return this.hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getSp() {
		return this.sp;
	}

	public void setSp(int sp) {
		this.sp = sp;
	}

	public int getAttack() {
		return this.attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getMaAttack() {
		return this.maAttack;
	}

	public void setMaAttack(int maAttack) {
		this.maAttack = maAttack;
	}

	public int getDefense() {
		return this.defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}

	public int getMaDefense() {
		return this.maDefense;
	}

	public void setMaDefense(int mDefense) {
		this.maDefense = mDefense;
	}

	public int getCri() {
		return this.cri;
	}

	public void setCri(int cri) {
		this.cri = cri;
	}

	public int getAvoid() {
		return this.avoid;
	}

	public void setAvoid(int avoid) {
		this.avoid = avoid;
	}

	public int getSpeed() {
		return this.speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getTurn() {
		return this.turn;
	}

	public void setTurn(int turn) {
		this.turn = turn;
	}

	public int getArmourGuard() {
		return this.armourGuard;
	}

	public void setArmourGuard(int armorGuird) {
		this.armourGuard = armorGuird;
	}

	public int getAmmo() {
		return this.ammo;
	}

	public void setAmmo(int ammo) {
		this.ammo = ammo;
	}

	public int getLimitBreak() {
		return this.limitBreak;
	}

	public void setLimitBreak(int limitBreak) {
		this.limitBreak = limitBreak;
	}

	public boolean isExtension() {
		return this.extension;
	}

	public void setExtension(boolean extension) {
		this.extension = extension;
	}

	public boolean isArmorBreak() {
		return this.armorBreak;
	}

	public void setArmorBreak(boolean armorBreak) {
		this.armorBreak = armorBreak;
	}

	public boolean isFireDamage() {
		return this.fireDamage;
	}

	public void setFireDamage(boolean fireDamage) {
		this.fireDamage = fireDamage;
	}

	public boolean isIceBind() {
		return this.iceBind;
	}

	public void setIceBind(boolean iceBind) {
		this.iceBind = iceBind;
	}

	public boolean isStan() {
		return this.stan;
	}

	public void setStan(boolean stan) {
		this.stan = stan;
	}

	public boolean isSilent() {
		return this.silent;
	}

	public void setSilent(boolean silent) {
		this.silent = silent;
	}

	public boolean isBind() {
		return this.bind;
	}

	public void setBind(boolean bind) {
		this.bind = bind;
	}

	public int getMaxHp() {
		return maxHp;
	}


	public void setMaxHp(int maxHp) {
		this.maxHp=maxHp;
	}


	public int getMaxSp() {
		return maxSp;
	}

	public void setMaxSp(int maxSp) {
		this.maxSp=maxSp;
	}

	public int getMaxArmourGuard() {
		return maxArmourGuard;
	}

	public void setMaxArmourGuard(int maxArmourGuird) {
		this.maxArmourGuard = maxArmourGuird;
	}

	public int getArmorBreakTurn() {
		return armorBreakTurn;
	}

	public void setArmorBreakTurn(int armorBreakTurn) {
		this.armorBreakTurn = armorBreakTurn;
	}

	public void eSelect(List<Player> player, List<Enemy> enemy) throws InterruptedException {
		// TODO 自動生成されたメソッド・スタブ

	}

	public void setBattle1(boolean b) {
		// TODO 自動生成されたメソッド・スタブ

	}

	public void setBattle2(boolean b) {
		// TODO 自動生成されたメソッド・スタブ

	}



}
