package lastBattle;

import java.util.List;

public abstract class Player {//プレイヤークラス　全ての味方キャラの基本
	private int cNum;//キャラクター番号;
	private String name;//名前
	private int select=0;
	private int skillNum;
	private boolean skillStandby=false;
	private int target=0;

	private int level;//レベル
	private int maxHp=0;
	private int maxSp=0;
	private int hp=getMaxHp();//体力。能力初期値は個々数値＋レベル分
	private int sp=getMaxSp();//スキルポイント
	private int attack;//物理攻撃力
	private int maAttack;//特殊攻撃力
	private int defense;//物理防御力
	private int maDefense;//特殊防御力
	private int cri;//クリティカル率
	private int avoid;//回避
	private int speed;//スピードはキャラレベルにより変化しない固有数値
	private int turn;//speedに応じて増加。行動で0へ
	private int defBreak;//アーマーブレイク値。敵の装甲を減らす
	private int ammo;//弾薬。戦闘の際、補給部隊が補充してくれる。

	//状態変更
	private int limitBreak;//リミットブレイク
	private boolean extension;//士気高揚状態
	private int counter;//カウンター回数

	//状態異常
	private boolean fireDamage;//炎上状態
	private boolean iceBind;//凍結状態
	private boolean stan;//スタン状態
	private boolean silent;//拘束状態




	//状態異常ガード
	public int goSelect(int select) throws InterruptedException {
		return select;
	}

	public int goSupport(List<Player> player) throws InterruptedException{


		return target;
	}


	public int goAttack(List<Enemy> enemy) throws InterruptedException{
		System.out.println(this.name+"の攻撃！");
		return target;
	}




	public int goSkill() throws InterruptedException{
		//選択肢から選択
		return 0;
	}


	public void goShot(){
		//残弾数
		//anyで連発
		//stopで停止
		//stop後行動を戻す
	}

	public void goGuird(){

	}

	public void goItem(){

	}




	public Player(String name) {//プレイヤーのコンストラクタ
		this.name=name;
		this.setHp(100);

	}





	public int getcNum() {
		return this.cNum;
	}
	public void setcNum(int cNum) {
		this.cNum = cNum;
	}

	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name=name;
	}
	public int getLevel() {
		return this.level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getHp() {
		return this.hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getSp() {
		return this.sp;
	}
	public void setSp(int sp) {
		this.sp = sp;
	}
	public int getAttack() {
		return this.attack;
	}
	public void setAttack(int attack) {
		this.attack = attack;
	}
	public int getMaAttack() {
		return this.maAttack;
	}
	public void setMaAttack(int maAttack) {
		this.maAttack = maAttack;
	}
	public int getDefense() {
		return this.defense;
	}
	public void setDefense(int defense) {
		this.defense = defense;
	}
	public int getMaDefense() {
		return this.maDefense;
	}
	public void setMaDefense(int mDefense) {
		this.maDefense = mDefense;
	}
	public int getCri() {
		return this.cri;
	}
	public void setCri(int cri) {
		this.cri = cri;
	}
	public int getAvoid() {
		return this.avoid;
	}
	public void setAvoid(int avoid) {
		this.avoid = avoid;
	}
	public int getSpeed() {
		return this.speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getTurn() {
		return this.turn;
	}
	public void setTurn(int turn) {
		this.turn = turn;
	}
	public int getDefBreak() {
		return this.defBreak;
	}
	public void setDefBreak(int defBreak) {
		this.defBreak = defBreak;
	}
	public int getAmmo() {
		return this.ammo;
	}
	public void setAmmo(int ammo) {
		this.ammo = ammo;
	}
	public int getLimitBreak() {
		return this.limitBreak;
	}
	public void setLimitBreak(int limitBreak) {
		this.limitBreak = limitBreak;
	}
	public boolean isExtension() {
		return this.extension;
	}
	public void setExtension(boolean extension) {
		this.extension = extension;
	}
	public boolean isFireDamage() {
		return this.fireDamage;
	}
	public void setFireDamage(boolean fireDamage) {
		this.fireDamage = fireDamage;
	}
	public boolean isIceBind() {
		return this.iceBind;
	}
	public void setIceBind(boolean iceBind) {
		this.iceBind = iceBind;
	}
	public boolean isStan() {
		return this.stan;
	}
	public void setStan(boolean stan) {
		this.stan = stan;
	}
	public boolean isSilent() {
		return this.silent;
	}
	public void setSilent(boolean silent) {
		this.silent = silent;
	}


	public int getMaxHp() {
		return maxHp;
	}


	public void setMaxHp(int maxHp) {
		this.maxHp=maxHp;
	}


	public int getMaxSp() {
		return maxSp;
	}

	public void setMaxSp(int maxSp) {
		this.maxSp=maxSp;
	}





	public void attackScript() throws InterruptedException {


	}

	public void toAvoidance() {
		// TODO 自動生成されたメソッド・スタブ

	}

	public void breakGo() {
		// TODO 自動生成されたメソッド・スタブ

	}

	public void breakGo2() {
		System.out.println(this.name+"");

	}

	public void orbBreak() {
		System.out.println();

	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public void goCounter() throws InterruptedException {
		// TODO 自動生成されたメソッド・スタブ

	}





}
