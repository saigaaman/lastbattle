package lastBattle;

import java.util.List;

public class QueenC{
	int select=0;

	/**
	 * @param enemy
	 * @param player
	 * @throws InterruptedException
	 */
	//クイン・シーの行動選択メソッド
	public void goSkill(List<Enemy> enemy, List<Player> player) throws InterruptedException {
		int select=new java.util.Random().nextInt(3)+1;
		switch(select){
			case 1:
				System.out.println("クイン・シーの強力支援！");
				Thread.sleep(1500);
				System.out.println("ターン無限＋重複可能な支援がパーティー全体にかけられた！");
				Thread.sleep(1500);
				for(int i=0;i<player.size();i++){
					player.get(i).setAttack((int)(player.get(i).getAttack()*1.2));
					player.get(i).setMaAttack((int)(player.get(i).getMaAttack()*1.2));
					player.get(i).setDefense((int)(player.get(i).getDefense()*1.2));
					System.out.println(player.get(i).getName()+"の攻撃力・防御力が上昇した！");
					Thread.sleep(2000);
				}
				break;
			case 2:
				System.out.println("クイン・シーの強力支援！");//
				Thread.sleep(1500);
				System.out.println("全てのメンバーのHPが最大値の30%回復し、戦闘不能キャラも復活！");
				Thread.sleep(1500);
				for(int i=0;i<player.size();i++){
					player.get(i).setHp(player.get(i).getHp()+(int)(player.get(i).getMaxHp()*0.3));
				}
				break;
			case 3:
				System.out.println("クイン・シーの精密射撃！");
				Thread.sleep(1500);
				System.out.println("敵全体にクリティカル攻撃！");
				Thread.sleep(1500);
				for(int i=0;i<enemy.size();i++){
					enemy.get(i).setHp((int)(enemy.get(i).getHp()-100000));
					System.out.println(enemy.get(i).getName()+"に100000ダメージ！");
					Thread.sleep(1500);
					if(enemy.get(i).getHp()<=0) {//もし敵を倒していたら
						System.out.println(enemy.get(i).getName()+"を倒した！");
						enemy.remove(i);
					}
				}
				break;
			default:
				break;
		}
	}
}
