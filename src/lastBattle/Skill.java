package lastBattle;


public class Skill {//全キャラのスキル計算を管理するクラス。
	//引数にて入ってきた各種パラメーターからダメージ等を算出し、各キャラへリターンする。
	//また、スキルはスキル属性ごとの管理とする。
	//attackSkill mAttackSkill supportSkill specialSkillの4種類。
	private int select=0;//選択するときif文とかに分岐させる際使う
	private int type=0;
	private int targetType=0;//敵の対象を選択する（攻撃用）
	private int targetRange=0;
	private int damage=0;//ダメージ（敵味方共用）
	private int skillType=0;
	private int num=0;//ダメージ以外の数値に
	private int supportType=0;
	private int[] supportPoint=new int[8];
	private int frequency=0;//回数（連続攻撃などに共用）
	private double mag=0;//数値の倍率（共用）
	private double buffMag;//バフ倍率
	private double deBuffMag;//デバフ倍率
	private double skillBreak=0;//スキルが持つ装甲破壊の倍率
	private int skillTurn=0;

	/*
	【幸一スキル】
	1『雑賀の霊弾』味方全員のattack・avoid ３ターン20%上昇　未実装
	2『煉獄』　　　威力SS 敵一体に多段攻撃 敵物理防御が高いとダメージの減少が激しい　実装済み
	3『絶影』　　　威力B 敵全体に防御無視攻撃　実装済み
	4『螺旋』　　　威力A 敵一体に物理攻撃+スタン確率20%　スタンだけ未実装

	【愛莉紗スキル】
	11『壱ノ太刀　風刃』　威力B　敵全体に２回連続攻撃＋遅延20　実装済み
	12『弐ノ太刀　羅刹』　威力A　自己強化スキル　敵の攻撃を２回まで直接攻撃回避＆カウンター　実装済み
	13『涼風』　　　　　　味方全体のHPを30%回復＋行動速度小アップ　実装済み
	14『参ノ太刀　災禍』　威力SS 敵一体にクリティカル攻撃し、稀に猛毒状態にする　猛毒は未実装

	【シロスキル】
	21『甘える』味方一人のHPを70%回復　実装済み
	22『踊る』　味方全体の防御力を5ターン30%上昇させる　未実装
	23『投げる』　威力A　光の矢を投げて敵全体にダメージ　実装済み
	24『起こす』味方一人の戦闘不能状態を回復（回復量は最大HPの50%）　実装済み

	【龍之介スキル】
	31『徹甲弾』威力A 敵一体に防御無視ダメージ＋敵装甲を大きく削り取る　実装済み
	32『煉獄弾』威力SS 敵全体にランダムでマシンガンを乱射する。敵防御が低い程ダメージ　実装済み
	33『金剛』　全身を硬化し、全身を硬化し、味方が戦闘不能となる攻撃を受けた際、一度だけ身代わりとなる　未実装
	34『破壊弾』威力S 炸裂する弾頭を使用し、敵全体にダメージ＋確率でスタン付与　スタン未実装

*/

	/**
	 * @param pSkillNum
	 * @throws InterruptedException
	 */
	//スキル使用時のセリフを表記
	public void skillScript(int pSkillNum) throws InterruptedException{
		switch(pSkillNum) {
		case 1://『雑賀の霊弾』味方全員のattack・avoid ３ターン20%上昇
			System.out.println("幸一『みんな、行くぞ！』");
			Thread.sleep(3000);
			System.out.println("『雑賀の霊弾』発動！味方全員に疑似弾を放ち、ステータスアップ！");
			Thread.sleep(3000);
			break;
		case 2://『煉獄』威力SS 敵一体に多段攻撃 敵物理防御が高いとダメージの減少が激しい
			System.out.println("幸一『これが、俺が与える貴様への……煉獄だ……！』");
			Thread.sleep(3000);
			System.out.println("『煉獄』発動！時の流れに囚われない神速の拳が雨のように放たれた！");
			Thread.sleep(3000);
			break;
		case 3://『絶影』威力B 敵全体に防御無視攻撃　※防御無視攻撃実装済み
			System.out.println("幸一『この攻撃、見切れると思うな！絶影！』");
			Thread.sleep(3000);
			System.out.println("『絶影』発動！拳から発せられる衝撃波が敵全てを飲み込む！");
			Thread.sleep(3000);
			break;
		case 4://『螺旋』威力A 敵一体に物理攻撃+スタン確率20%
			System.out.println("幸一『この攻撃、受けられるものなら受けてみろ！』");
			Thread.sleep(3000);
			System.out.println("『螺旋』発動！あらぶる拳がうなり声を挙げ、襲い掛かる！");
			Thread.sleep(3000);
			break;
		case 5://マキシマムドライブ
			System.out.println("幸一『これを選んでしまったのか・・・』");
			Thread.sleep(3000);
			System.out.println("『ジョーカーエクストリーム』発動！");
			Thread.sleep(3000);
			break;
		case 11://『壱ノ太刀　風刃』威力B　敵全体に２回連続攻撃＋遅延20　※遅延実装済み
			System.out.println("愛莉紗『うふふ……この真空の刃から逃れる事は出来ませんよ』");
			Thread.sleep(3000);
			System.out.println("『風刃』発動！刀から二対の風圧が敵全体を切り刻む！");
			Thread.sleep(3000);
			break;
		case 12://『弐ノ太刀　羅刹』　威力A　自己強化スキル　敵の攻撃を２回まで直接攻撃回避＆カウンター
			System.out.println("愛莉紗『この愛莉紗の妙技をお見せ致しましょう！』");
			Thread.sleep(3000);
			System.out.println("『羅刹』発動！愛莉紗は羅刹状態に入った！");
			Thread.sleep(3000);
			break;
		case 13://『涼風』味方全体のHPを30%回復＋味方の行動速度10アップ
			System.out.println("愛莉紗『皆さま、ここが踏ん張りどころです！』");
			Thread.sleep(3000);
			System.out.println("『涼風』発動！涼やかなる風が味方を包み込む！");
			Thread.sleep(3000);
			break;
		case 14://『参ノ太刀　災禍』威力SS 敵一体に確定クリティカル攻撃し、稀に猛毒状態にする
			System.out.println("愛莉紗『全ては坊ちゃまの為に……これが愛莉紗の奥義です！』");
			Thread.sleep(3000);
			System.out.println("『災禍』発動！刃先に火死毒を塗り、不可視の斬撃を放った！");
			Thread.sleep(3000);
			break;
		case 15://ファイナル弁当
			System.out.println("愛莉紗『これは、坊ちゃまに食べて頂こうとしていたお弁当でございます』");
			Thread.sleep(3000);
			System.out.println("『ファイナル弁当』発動！愛莉紗は小石に躓き、弁当を敵全体にぶちまけた！");
			Thread.sleep(3000);
			break;
		case 21://『甘える』味方一人のHPを70%回復
			System.out.println("シロ「にゃー！ごろごろ、にゃっ」");
			Thread.sleep(3000);
			System.out.println("『甘える』発動！味方一人を狙いすませて、とにかく甘えた！");
			Thread.sleep(3000);
			break;
		case 22://『踊る』　味方全体の防御力を5ターン30%上昇させる
			System.out.println("シロ『みんなハッスルハッスルにゃ』");
			Thread.sleep(3000);
			System.out.println("『踊る』発動！シロは元気が出る踊りを踊った！");
			Thread.sleep(3000);
			break;
		case 23://『投げる』　威力A　敵全体に光の矢を投げてダメージ
			System.out.println("シロ『よくわからない光ってる矢をぶつけるにゃ！』");
			Thread.sleep(3000);
			System.out.println("『投げる』発動！シロはよくわからない光の矢を敵全体に放った！");
			Thread.sleep(3000);
			break;
		case 24://『起こす』味方一人の戦闘不能状態を回復（回復量は最大HPの50%）
			System.out.println("シロ『寝たら死ぬにゃ！起きるにゃあああ！』");
			Thread.sleep(3000);
			System.out.println("『起こす』発動！シロの全身全霊の癒しが放たれる！");
			Thread.sleep(3000);
			break;
		case 25://『スーパー大切断』
			System.out.println("シロ『爪を研いできたにゃ！まとめてひっかくにゃ。』");
			Thread.sleep(3000);
			System.out.println("『スーパー大切断』発動！研ぎ澄まされたシロの爪が襲い掛かる！");
			Thread.sleep(3000);
			break;
		case 31://『徹甲弾』威力A 敵一体に防御無視ダメージ＋敵装甲を大きく削り取る
			System.out.println("龍之介『その防備、破らせて貰おう！』");
			Thread.sleep(3000);
			System.out.println("『徹甲弾』発動！装甲を大きく削り取る弾丸が放たれた！");
			Thread.sleep(3000);
			break;
		case 32://『煉獄弾』威力SS 敵全体にランダムでマシンガンを乱射する。敵防御が低い程ダメージ
			System.out.println("龍之介『覚悟は出来たか？俺は出来てる』");
			Thread.sleep(3000);
			System.out.println("『煉獄弾』発動！雨のように弾丸が放たれた！");
			Thread.sleep(3000);
			break;

		case 33://『金剛』全身を硬化し、味方が戦闘不能となる攻撃を受けた際、一度だけ身代わりとなる
			System.out.println("龍之介『任せておけ、誰も死なせはしない！』");
			Thread.sleep(3000);
			System.out.println("『金剛』発動！龍之介は金剛状態に入った！");
			Thread.sleep(3000);
			break;
		case 34://『破壊弾』威力S 炸裂する弾頭を使用し、敵全体にダメージ＋確率でスタン付与
			System.out.println("龍之介『フン、とっておきの弾だ。一人たりとも逃すものか！』");
			Thread.sleep(3000);
			System.out.println("『破壊弾』発動！着弾した弾丸が強く輝き、大爆発を引き起こす！");
			Thread.sleep(3000);
			break;
		case 35://リボルクラッシュ
			System.out.println("龍之介『ゆるさん！』");
			Thread.sleep(3000);
			System.out.println("『リボルクラッシュ』発動！突如として取り出された光の剣を手に、龍之介が敵全体に斬撃を放った！");
			Thread.sleep(3000);
			break;
		default:
			break;
		}
	}




	/**
	 * @param pSkillNum
	 * @return
	 */
	//スキルのタイプを返すメソッド
	public int skillType(int pSkillNum) {

		switch(pSkillNum) {
			case 2://『煉獄』威力SS 敵一体に多段攻撃 敵物理防御が高いとダメージの減少が激しい
			case 3://『絶影』威力B 敵全体に防御無視攻撃
			case 4://『螺旋』威力A 敵一体に物理攻撃+スタン確率20%
			case 5://ジョーカーエクストリーム
			case 11://『壱ノ太刀　風刃』威力B　敵全体に２回連続攻撃＋遅延20
			case 14://『参ノ太刀　災禍』威力SS 敵一体にクリティカル攻撃し、稀に猛毒状態にする
			case 15://ファイナル弁当
			case 25://大切断
			case 31://『徹甲弾』威力A 敵一体に防御無視ダメージ＋敵装甲を大きく削り取る
			case 32://『煉獄弾』威力SS 敵全体にランダムでマシンガンを乱射する。敵防御が低い程ダメージ
			case 34://『破壊弾』威力S 炸裂する弾頭を使用し、敵全体にダメージ＋確率でスタン付与
			case 35://リボルクラッシュ
				skillType=1;//攻撃スキル。attack依存で対象は敵のみ
				break;
			case 23://『投げる』　威力A　光の矢を投げて敵全体にダメージ
				skillType=2;//魔法攻撃スキル。mAttack依存で対象は敵のみ
				break;
			case 13://『涼風』味方全体のHPを30%回復＋2ターン行動速度小アップ
			case 21://『甘える』味方一人のHPを70%回復
			case 24://『起こす』味方一人の戦闘不能状態を回復（回復量は最大HPの50%）
				skillType=3;//補助スキル。対象は敵も味方もあり
				break;
			case 1://『雑賀の霊弾』味方全員のattack・avoid ３ターン20%上昇
			case 22://『踊る』　味方全体の防御力を5ターン30%上昇させる
				skillType=4;
				break;
			case 12://『弐ノ太刀　羅刹』　威力A　自己強化スキル　敵の攻撃を２回まで直接攻撃回避＆カウンター
			case 33://全身を硬化し、味方が戦闘不能となる攻撃を受けた際、一度だけ身代わりとなる
				skillType=5;//特殊スキル。対象は自分のみ
				break;
			default:
				break;
		}
		return skillType;
	}


	/**
	 * @param pSkillNum
	 * @return
	 */
	//スキルのターゲットを返すメソッド。
	public int skillTarget(int pSkillNum) {
		//target 1=敵　2=味方　3=自己
		switch(pSkillNum) {
			case 2://『煉獄』威力SS 敵一体に多段攻撃 敵物理防御が高いとダメージの減少が激しい
			case 3://『絶影』威力B 敵全体に防御無視攻撃
			case 4://『螺旋』威力A 敵一体に物理攻撃+スタン確率20%
			case 5://ジョーカーエクストリーム
			case 11://『壱ノ太刀　風刃』威力B　敵全体に２回連続攻撃＋遅延20
			case 14://『参ノ太刀　災禍』威力SS 敵一体にクリティカル攻撃し、稀に猛毒状態にする
			case 15://ファイナル弁当
			case 23://『投げる』　威力A　光の矢を投げて敵全体にダメージ
			case 25://大切断
			case 31://『徹甲弾』威力A 敵一体に防御無視ダメージ＋敵装甲を大きく削り取る
			case 32://『煉獄弾』威力SS 敵全体にランダムでマシンガンを乱射する。敵防御が低い程ダメージ
			case 34://『破壊弾』威力S 炸裂する弾頭を使用し、敵全体にダメージ＋確率でスタン付与
			case 35://リボるクラッシュ
				targetType=1;//攻撃スキルおよび敵への補助系は対象が敵
				break;
			case 1://『雑賀の霊弾』味方全員のattack・avoid ３ターン20%上昇
			case 13://『涼風』味方全体のHPを30%回復＋2ターン行動速度小アップ
			case 21://『甘える』味方一人のHPを70%回復
			case 22://『踊る』　味方全体の防御力を5ターン30%上昇させる
			case 24://『起こす』味方一人の戦闘不能状態を回復（回復量は最大HPの50%）
				targetType=2;//味方への補助系・回復系は味方
				break;
			case 12://『弐ノ太刀　羅刹』　威力A　自己強化スキル　敵の攻撃を２回まで直接攻撃回避＆カウンター
			case 33://『金剛』全身を硬化し、味方が戦闘不能となる攻撃を受けた際、一度だけ身代わりとなる
				targetType=3;//特殊スキル。対象は自分のみ
				break;
			default:
				targetType=4;//敵・味方含む全体(未実装)
				break;
		}
		return targetType;
	}

	/**
	 * @param pSkillNum
	 * @return
	 */
	//スキルのターゲット範囲を返すメソッド
	public int skillRange(int pSkillNum) {

		switch(pSkillNum) {
			case 2://『煉獄』威力SS 敵一体に多段攻撃 敵物理防御が高いとダメージの減少が激しい
			case 4://『螺旋』威力A 敵一体に物理攻撃+スタン確率20%
			case 14://『参ノ太刀　災禍』威力SS 敵一体にクリティカル攻撃し、稀に猛毒状態にする
			case 21://『甘える』味方一人のHPを70%回復
			case 24://『起こす』味方一人の戦闘不能状態を回復（回復量は最大HPの50%）
			case 31://『徹甲弾』威力A 敵一体に防御無視ダメージ＋敵装甲を大きく削り取る
				targetRange=1;//単体・選択式
				break;
			case 1://『雑賀の霊弾』味方全員のattack・avoid ３ターン20%上昇
			case 3://『絶影』威力B 敵全体に防御無視攻撃
			case 5://ジョーカーエクストリーム
			case 11://『壱ノ太刀　風刃』威力B　敵全体に２回連続攻撃＋遅延20
			case 13://『涼風』味方全体のHPを30%回復＋2ターン行動速度小アップ
			case 15://ファイナル弁当
			case 22://『踊る』　味方全体の防御力を5ターン30%上昇させる
			case 23://『投げる』　威力A　敵全体に光の矢を投げてダメージ
			case 25://大切断
			case 34://『破壊弾』威力S 炸裂する弾頭を使用し、敵全体にダメージ＋確率でスタン付与
			case 35://リボるクラッシュ
				targetRange=2;//全体・選択不可
				break;
			case 32://『煉獄弾』威力SS 敵全体にランダムでマシンガンを乱射する。敵防御が低い程ダメージ
				targetRange=3;//敵内ランダム式
				break;
			case 12://『弐ノ太刀　羅刹』　威力A　自己強化スキル　敵の攻撃を２回まで直接攻撃回避＆カウンター
			case 33://『金剛』全身を硬化し、味方が戦闘不能となる攻撃を受けた際、一度だけ身代わりとなる
				targetRange=4;//自己・選択不可
				break;
			default:
				break;
		}
		return targetRange;
	}

	/**
	 * @param pSkillNum
	 * @return
	 */
	//スキルの倍率を返す（100%基準）
	public double skillMag(int pSkillNum) {
		switch(pSkillNum) {
			case 1://『雑賀の霊弾』味方全員のattack・avoid ３ターン20%上昇
				mag=120;
				break;
			case 2://『煉獄』威力SS 敵一体に多段攻撃 敵物理防御が高いとダメージの減少が激しい
				mag=35;
				break;
			case 3://『絶影』威力B 敵全体に防御無視攻撃
				mag=140;
				break;
			case 4://『螺旋』威力A 敵一体に物理攻撃+スタン確率20%
				mag=300;
				break;
			case 5://ジョーカーエクストリーム
				mag=50000;
				break;
			case 11://『壱ノ太刀　風刃』威力B　敵全体に２回連続攻撃＋遅延20
				mag=85;
				break;
			case 12://『弐ノ太刀　羅刹』　威力A　自己強化スキル　敵の攻撃を２回まで直接攻撃回避＆カウンター
				mag=185;
				break;
			case 13://『涼風』味方全体のHPを30%回復＋2ターン行動速度10アップ
				mag=30;
				break;
			case 14://『参ノ太刀　災禍』威力SS 敵一体にクリティカル攻撃し、稀に猛毒状態にする
				mag=550;
				break;
			case 15://ファイナル弁当
				mag=50000;
				break;
			case 21://『甘える』味方一人のHPを70%回復
				mag=70;
				break;
			case 22://『踊る』　味方全体の防御力を5ターン30%上昇させる
				mag=0;
				break;
			case 23://『投げる』　威力A　敵全体に光の矢を投げてダメージ
				mag=170;
				break;
			case 24://『起こす』味方一人の戦闘不能状態を回復（回復量は最大HPの50%）
				mag=50;
				break;
			case 25://大切断
				mag=50000;
				break;
			case 31://『徹甲弾』威力A 敵一体に防御無視ダメージ＋敵装甲を大きく削り取る
				mag=200;
				break;
			case 32://『煉獄弾』威力SS 敵全体にランダムでマシンガンを乱射する。敵防御が低い程ダメージ
				mag=35;
				break;
			case 33://『金剛』全身を硬化し、味方が戦闘不能となる攻撃を受けた際、一度だけ身代わりとなる
				mag=200;
				break;
			case 34://『破壊弾』威力S 炸裂する弾頭を使用し、敵全体にダメージ＋確率でスタン付与
				mag=380;
				break;
			case 35://リボるクラッシュ
				mag=50000;
				break;
			default:
				break;
		}
		return mag;
	}


	/**
	 * @param pSkillNum
	 * @return
	 */
	//スキルによるアーマーブレイク値を返すメソッド
	public double skillBreak(int pSkillNum) {
		switch(pSkillNum) {
			case 2://『煉獄』威力SS 敵一体に多段攻撃 敵物理防御が高いとダメージの減少が激しい
				skillBreak=0.01;
				break;
			case 3://『絶影』威力B 敵全体に防御無視攻撃
				skillBreak=1.2;
				break;
			case 4://『螺旋』威力A 敵一体に物理攻撃+スタン確率20%
				skillBreak=1.3;
				break;
			case 5://ジョーカーエクストリーム
				skillBreak=1;
				break;
			case 11://『壱ノ太刀　風刃』威力B　敵全体に２回連続攻撃＋遅延20
				skillBreak=1.1;
				break;
			case 12://『弐ノ太刀　羅刹』　威力A　自己強化スキル　敵の攻撃を２回まで直接攻撃回避＆カウンター
				skillBreak=1.3;
				break;
			case 14://『参ノ太刀　災禍』威力SS 敵一体にクリティカル攻撃し、稀に猛毒状態にする
				skillBreak=1.8;
				break;
			case 15://ふぁいなる弁当
				skillBreak=1;
				break;
			case 23://『投げる』　威力A　敵全体に光の矢を投げてダメージ
				skillBreak=1.2;
				break;
			case 25://大切断
				skillBreak=1;
				break;
			case 31://『徹甲弾』威力A 敵一体に防御無視ダメージ＋敵装甲を大きく削り取る
				skillBreak=3.0;
				break;
			case 32://『煉獄弾』威力SS 敵全体にランダムでマシンガンを乱射する。敵防御が低い程ダメージ
				skillBreak=0.1;
				break;
			case 34://『破壊弾』威力S 炸裂する弾頭を使用し、敵全体にダメージ＋確率でスタン付与
				skillBreak=2.0;
				break;
			case 35://リボるクラッシュ
				skillBreak=1.0;
				break;
			default:
				skillBreak=0;
				break;
		}
		return skillBreak;
	}


	/**
	 * @param pSkillNum
	 * @return
	 */
	//スキルの回数を返すメソッド
	public int skillfrequency(int pSkillNum) {
		switch(pSkillNum) {
			case 5:
			case 11://『壱ノ太刀　風刃』威力B　敵全体に２回連続攻撃＋遅延20
			case 12://『弐ノ太刀　羅刹』　威力A　自己強化スキル　敵の攻撃を２回まで直接攻撃回避＆カウンター
			case 15:
			case 25:
			case 35:
				frequency=2;
				break;
			case 2://『煉獄』威力SS 敵一体に多段攻撃 敵物理防御が高いとダメージの減少が激しい
				frequency=new java.util.Random().nextInt(11)+5;//最大15回連続攻撃、最低5回
				break;
			case 32://『煉獄弾』威力SS 敵全体にランダムでマシンガンを乱射する。敵防御が低い程ダメージ
				frequency=new java.util.Random().nextInt(9)+7;//最大15回連続攻撃、最低7回
				break;
			default:
				frequency=1;
				break;
		}
		return frequency;
	}

	/**
	 * @param pSkillNum
	 * @return
	 */
	//スキルの効果ターンを返すメソッド　デバフ・バフ・dotダメージ
	public int skillTurn(int pSkillNum) {
		switch(pSkillNum) {
			case 1://『雑賀の霊弾』味方全員のattack・avoid ３ターン20%上昇
				break;
			case 13://『涼風』味方全体のHPを30%回復＋2ターン行動速度10アップ
				break;
			case 22://『踊る』　味方全体の防御力を5ターン30%上昇させる
				break;
			default:
				skillTurn=0;//ターン持続系のスキルでなければターンは０とする
				break;
		}
		return skillTurn;
	}
	/**
	 * @param pSkillNum
	 * @return
	 */
	//スキルに付与されるバフ数値を返す
	public int[] supportPoint(int pSkillNum){
		supportPoint[0]=0;//このスキルが呼ばれる度に、全てのサポートポイントがリセット
		supportPoint[1]=0;
		supportPoint[2]=0;
		supportPoint[3]=0;
		supportPoint[4]=0;
		supportPoint[5]=0;
		supportPoint[6]=0;
		supportPoint[7]=0;
		//supportPoint[]={hp,sp,attack,maAttack,def,mDef,cri,avoid,speed}
		switch(pSkillNum){
			case 1://『雑賀の霊弾』味方全員のattack・avoid ３ターン20%上昇
			case 13://『涼風』味方全体のHPを30%回復＋2ターン行動速度小アップ
			case 21://『甘える』味方一人のHPを70%回復
			case 22://『踊る』　味方全体の防御力を5ターン30%上昇させる
			case 24://『起こす』味方一人の戦闘不能状態を回復（回復量は最大HPの50%）
				break;
				//魔法攻撃依存ならsupportType=1にする
			default:
				break;
		}
		return supportPoint;
	}

	/**
	 * @param pSkillNum
	 * @return
	 */
	//サポートスキルのタイプ
	public int supportType(int pSkillNum){
		switch(pSkillNum){
			case 1://『雑賀の霊弾』味方全員のattack・avoid ３ターン20%上昇
			case 13://『涼風』味方全体のHPを30%回復＋2ターン行動速度小アップ
			case 21://『甘える』味方一人のHPを70%回復
			case 22://『踊る』　味方全体の防御力を5ターン30%上昇させる
			case 24://『起こす』味方一人の戦闘不能状態を回復（回復量は最大HPの50%）
				supportType=0;//割合方式
				break;
				//魔法攻撃依存ならsupportType=1にする
			default:
				break;
		}
		return supportType;
	}

	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}



	public int getDamage() {
		return damage;
	}
	public void setDamage(int damage) {
		this.damage = damage;
	}
	public int getSelect() {
		return select;
	}
	public void setSelect(int select) {
		this.select = select;
	}
	public int getTarget() {
		return targetType;
	}
	public void setTarget(int target) {
		this.targetType = targetType;
	}



	public int getSkillType() {
		return skillType;
	}
	public void setSkillType(int skillType) {
		this.skillType = skillType;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getFrequency() {
		return frequency;
	}
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	public double getMag() {
		return mag;
	}
	public void setMag(double mag) {
		this.mag = mag;
	}





	public Skill(int select) {
		this.select=0;
		this.type=0;
		this.targetType=0;//敵の対象を選択する（攻撃用）
		this.damage=0;//ダメージ（敵味方共用）
		this.skillType=0;
		this.num=0;//ダメージ以外の数値に
		this.frequency=0;//回数（連続攻撃などに共用）
		this.mag=0;

	}


	public int getTargetRange() {
		return targetRange;
	}


	public void setTargetRange(int targetRange) {
		this.targetRange = targetRange;
	}


	public int getSkillTurn() {
		return skillTurn;
	}


	public void setSkillTurn(int skillTurn) {
		this.skillTurn = skillTurn;
	}




	public double getBuffMag() {
		return buffMag;
	}




	public void setBuffMag(double buffMag) {
		this.buffMag = buffMag;
	}




	public double getDeBuffMag() {
		return deBuffMag;
	}




	public void setDeBuffMag(double deBuffMag) {
		this.deBuffMag = deBuffMag;
	}




























}
