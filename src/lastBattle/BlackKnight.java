package lastBattle;

import java.util.List;

public class BlackKnight extends Enemy{//ラスボスである黒騎士の管理クラス。
	//Enemyクラスを引き継いでいる。
	private int attackFrequency=0;//攻撃回数
	private int eSelectNum=0;//スキル番号
	private int target=0;//ターゲット
	private int mag=0;//黒騎士の攻撃倍率
	private int random=0;//乱数
	private int damage=0;//ダメージ
	private int critical=0;//クリティカル有無
	private int Avoidance=0;//回避可否
	public int frequency=0;//攻撃回数
	private boolean battle1=false;//オーブ破壊してから
	private boolean battle2=false;//クイン・シー参戦後
	private boolean targeSuccess=false;//ターゲット選択成功有無



	/* (非 Javadoc)
	 * @see lastBattle.Enemy#eSelect(java.util.List, java.util.List)
	 */
	//敵の攻撃選択
	public void eSelect(List<Player> player, List<Enemy> enemy) throws InterruptedException {//ブラックナイトの攻撃分岐
		attackFrequency=new java.util.Random().nextInt(3)+1;//最大3回攻撃

		if(battle1==true){//もしオーブ破壊後なら
			attackFrequency=new java.util.Random().nextInt(2)+2;//確定2回の最大3回攻撃
		}else if(battle2==true){//ラストバトルなら
			attackFrequency=new java.util.Random().nextInt(5)+1;//最大5回攻撃
		}
		System.out.println("黒騎士の攻撃回数は"+attackFrequency);

		for(int i=0;i<attackFrequency;i++) {
				if(battle1==true){//オーブ破壊状態かつ、クイン・シー参戦前
					eSelectNum=new java.util.Random().nextInt(5)+1;//6通りの攻撃パターン（実質5）
					if(eSelectNum==3){//破壊後は召喚できない
						i--;
						continue;
					}
				}else if(battle2==true){//クイン・シー参戦後
					eSelectNum=new java.util.Random().nextInt(8)+1;//8通りの攻撃パターン（実質5）
					//かつ、7と8が出たら攻撃が妨害される
					if(eSelectNum==3){//破壊後は召喚できない
						i--;
						continue;
					}
				}else{//初期状態
					eSelectNum=new java.util.Random().nextInt(4)+1;//3通りの攻撃パターン
					if(eSelectNum==4){
						eSelectNum=3;
					}
				}

				while(enemy.size()==3&&eSelectNum==3){
					System.out.println(this.getName()+"『いでよ、我が忠実なる兵隊よ！』");
					Thread.sleep(1500);
					System.out.println(this.getName()+"の『コール・ファントムナイト』！");
					Thread.sleep(1500);
					System.out.println("しかしこれ以上召喚できない！");
					Thread.sleep(2000);
					eSelectNum=new java.util.Random().nextInt(2)+1;//最大3回攻撃
				}//無限召喚回避用

			switch(eSelectNum) {
				case 1://全体弱攻撃（物理）
					cutOff(player,enemy);//薙ぎ払いスキルメソッドへ
					break;
				case 2://全体強攻撃（魔法攻撃・物理攻撃のミックス）
					bloodyCross(player,enemy);
					break;
				case 3://分身を呼ぶ
						callAvatar(player,enemy);
					break;
				case 4://ランダム攻撃(物理)
					thrustFang(player ,enemy);
					break;
				case 5://単体大ダメージ攻撃
					mailstrom(player, enemy);
					break;
				case 6://単体超ダメージ攻撃
					worldDestruction(player,enemy);
					break;
				case 7://クイン・シーによる妨害
					System.out.println(this.getName()+"『愚か者め、引導を渡してやる！』");
					Thread.sleep(2000);
					System.out.println("クイン・シーのバックショット発動！");
					Thread.sleep(2000);
					System.out.println("【黒騎士の行動、無力化！】");
					Thread.sleep(2000);
					break;
				case 8:
					System.out.println(this.getName()+"『どう足掻こうが、貴様らの負けだ！』");
					Thread.sleep(2000);
					System.out.println("クイン・シーのバックショット発動！");
					Thread.sleep(2000);
					System.out.println("【黒騎士の行動、無力化！】");
					Thread.sleep(2000);
					break;
				default:
					System.out.println("おかしな数値出てますよ");
					break;
			}
		}




	}

	/**
	 * @param player
	 * @param enemy
	 * @throws InterruptedException
	 */
	//ワールドディストラクション
	public void worldDestruction(List<Player> player, List<Enemy> enemy) throws InterruptedException {
		mag=300;
		targeSuccess=false;
		Avoidance=new java.util.Random().nextInt(100)+1;
		while(targeSuccess==false){
			target=new java.util.Random().nextInt(player.size());//プレイヤーの数だけランダム要素
			if(player.get(target).getHp()>=0){//もし対象が死んでいなければ
				targeSuccess=true;//ターゲット成功
			}
		}
		System.out.println(this.getName()+"『もはや、後戻りはできぬ！』");
		Thread.sleep(1500);
		System.out.println(this.getName()+"の『ワールド・ディストラクション』！");
		Thread.sleep(1500);
		System.out.println(this.getName()+"の魔剣が全ての力を開放し、"+player.get(target).getName()+"に襲い掛かる！");
		random=new java.util.Random().nextInt(31)+85;//ランダムで85～115を出す
		damage=(int)((this.getAttack()*5)-(player.get(target).getDefense()*2.5)
				*0.5);
		damage=(int)(damage*mag/100)*random/100;//ダメージをさらにスキル倍率やランダム倍率
		critical=new java.util.Random().nextInt(101);//0～100でランダム抽選
		if(this.getCri()>=critical) {//もしキャラのクリティカルにマッチしていたら
			damage=damage*2;//ダメージ２倍
			System.out.print("クリティカル攻撃！");
			Thread.sleep(1500);
		}
		if(player.get(target).getCounter()>0) {//もしカウンター状態なら
			player.get(target).goCounter();
			this.setHp(this.getHp()- damage);//ターゲットのHPを減らす。
			System.out.println(this.getName()+"に"+damage+"のダメージ！");
			Thread.sleep(1500);
			player.get(target).setCounter(player.get(target).getCounter()-1);//カウンターを減らす
		}else if(player.get(target).getAvoid()>=Avoidance&&this.getCri()<=critical) {//もしクリティカルじゃなければ回避可能
			player.get(target).toAvoidance();
			System.out.println(player.get(target).getName()+"は攻撃を回避した！");
			Thread.sleep(1500);
		}else{
		damage=damage/10;//強すぎるので調整1/10
		player.get(target).setHp(player.get(target).getHp() - damage);//ターゲットのHPを減らす。
		System.out.println(player.get(target).getName()+"に"+damage+"のダメージ！");
		Thread.sleep(1500);
		}
		if(player.get(target).getHp()<=0){
			player.get(target).setHp(0);
			System.out.println(player.get(target).getName()+"は戦闘不能になった！");
			Thread.sleep(1500);
		}


	}
	/**
	 * @param player
	 * @param enemy
	 * @throws InterruptedException
	 */
	//メイルシュトローム
	public void mailstrom(List<Player> player, List<Enemy> enemy) throws InterruptedException {
		mag=250;
		targeSuccess=false;
		Avoidance=new java.util.Random().nextInt(100)+1;//回避の可否
		while(targeSuccess==false){
			target=new java.util.Random().nextInt(player.size());//プレイヤーの数だけランダム要素
			if(player.get(target).getHp()>=0){//もし対象が死んでいなければ
				targeSuccess=true;//ターゲット成功
			}
		}
		System.out.println(this.getName()+"『愚か者め、引導を渡してやる！』");
		Thread.sleep(1500);
		System.out.println(this.getName()+"の『メイルシュトローム』！");
		Thread.sleep(1500);
		System.out.println(this.getName()+"より発せられた漆黒の闘気が"+player.get(target).getName()+"に襲い掛かる！");
		random=new java.util.Random().nextInt(31)+85;//ランダムで85～115を出す
		damage=(int)((this.getAttack()*5)-(player.get(target).getDefense()*2.5)
				*0.5);
		damage=(int)(damage*mag/100)*random/100;//ダメージをさらにスキル倍率やランダム倍率
		critical=new java.util.Random().nextInt(101);//0～100でランダム抽選
		if(this.getCri()>=critical) {//もしキャラのクリティカルにマッチしていたら
			damage=damage*2;//ダメージ２倍
			System.out.print("クリティカル攻撃！");
			Thread.sleep(1500);
		}if(player.get(target).getCounter()>0) {//もしカウンター状態なら
			player.get(target).goCounter();
			this.setHp(this.getHp() - damage);//ターゲットのHPを減らす。
			System.out.println(this.getName()+"に"+damage+"のダメージ！");
			Thread.sleep(1500);
			player.get(target).setCounter(player.get(target).getCounter()-1);//カウンターを減らす

		}else if(player.get(target).getAvoid()>=Avoidance&&this.getCri()<=critical) {//もしクリティカルじゃなければ回避可能
			player.get(target).toAvoidance();
			System.out.println(player.get(target).getName()+"は攻撃を回避した！");
			Thread.sleep(1500);
		}else{
		damage=damage/10;//強すぎるので調整1/10
		player.get(target).setHp(player.get(target).getHp() - damage);//ターゲットのHPを減らす。
		System.out.println(player.get(target).getName()+"に"+damage+"のダメージ！");
		Thread.sleep(1500);
			if(player.get(target).getHp()<=0){
				player.get(target).setHp(0);
				System.out.println(player.get(target).getName()+"は戦闘不能になった！");
				Thread.sleep(1500);
			}
		}


	}
	/**
	 * @param player
	 * @param enemy
	 * @throws InterruptedException
	 */
	//スラストファング
	public void thrustFang(List<Player> player, List<Enemy> enemy) throws InterruptedException {
		mag=80;
		frequency=new java.util.Random().nextInt(3)+2;//最大4回攻撃
		System.out.println(this.getName()+"『どう足掻こうが、貴様らの負けだ！』");
		Thread.sleep(1500);
		System.out.println(this.getName()+"の『スラスト・ファング』！");
		Thread.sleep(1500);
		System.out.println(this.getName()+"の槍が目にも止まらぬ速さで襲い掛かる！");
		Thread.sleep(1500);
		for(int a=0;a<frequency;a++) {//スキルの攻撃回数分、ダメージ繰り返し
			Avoidance=new java.util.Random().nextInt(100)+1;//回避の可否
			if(player.size()==1){//敵が一人ならターゲットは0固定
				target=0;
			}else{//敵が二人以上ならランダム選択し、-1
				target=new java.util.Random().nextInt(player.size());//敵の数だけランダム要素
			}
			if(player.get(target).getHp()<=0){//もし対象が死んでいたら
				a--;
				continue;//何もせず次へ
			}
			random=new java.util.Random().nextInt(31)+85;//ランダムで85～115を出す
			damage=(int)((this.getAttack()*5)-(player.get(target).getDefense()*2.5)
					*0.5);
			damage=(int)(damage*mag/100)*random/100;//ダメージをさらにスキル倍率やランダム倍率
			critical=new java.util.Random().nextInt(101);//0～100でランダム抽選
			if(this.getCri()>=critical) {//もしキャラのクリティカルにマッチしていたら
				damage=damage*2;//ダメージ２倍
				System.out.print("クリティカル攻撃！");
				Thread.sleep(1500);
			}if(player.get(target).getCounter()>0) {//もしカウンター状態なら
				player.get(target).goCounter();
				this.setHp(this.getHp() - damage);//ターゲットのHPを減らす。
				System.out.println(this.getName()+"に"+damage+"のダメージ！");
				Thread.sleep(1500);
				player.get(target).setCounter(player.get(target).getCounter()-1);//カウンターを減らす
				continue;
			}else if(player.get(target).getAvoid()>=Avoidance&&this.getCri()<=critical) {//もしクリティカルじゃなければ回避可能
				player.get(target).toAvoidance();
				System.out.println(player.get(target).getName()+"は攻撃を回避した！");
				Thread.sleep(1500);
				continue;
			}
			damage=damage/10;//強すぎるので調整1/10
			player.get(target).setHp(player.get(target).getHp() - damage);//ターゲットのHPを減らす。
			System.out.println(player.get(target).getName()+"に"+damage+"のダメージ！");
			Thread.sleep(1500);
			if(player.get(target).getHp()<=0){
				player.get(target).setHp(0);
				System.out.println(player.get(target).getName()+"は戦闘不能になった！");
				Thread.sleep(1500);
			}
		}
	}


	/**
	 * @param player
	 * @param enemy
	 * @throws InterruptedException
	 */
	//召喚
	public void callAvatar(List<Player> player, List<Enemy> enemy) throws InterruptedException {
		if(enemy.size()==1){
			Guard guard=new Guard("ミラージュナイト");
			System.out.println(this.getName()+"『いでよ、我が忠実なる兵隊よ！』");
			Thread.sleep(1500);
			System.out.println(this.getName()+"の『コール・ミラージュナイト』！");
			Thread.sleep(1500);
			enemy.add(guard);
			enemy.get(1).setName("ミラージュナイト");
			System.out.println(this.getName()+"の分身"+enemy.get(1).getName()+"が召喚された！");
			Thread.sleep(1500);
		}else if(enemy.size()==2){
			Guard guard=new Guard("ファントムナイト");
			System.out.println(this.getName()+"『いでよ、我が忠実なる兵隊よ！』");
			Thread.sleep(1500);
			System.out.println(this.getName()+"の『コール・ファントムナイト』！");
			Thread.sleep(1500);
			enemy.add(guard);
			enemy.get(2).setName("ファントムナイト");
			System.out.println(this.getName()+"の分身"+enemy.get(2).getName()+"が召喚された！");
			Thread.sleep(1500);
		}

	}


	/**
	 * @param player
	 * @param enemy
	 * @throws InterruptedException
	 */
	//ブラッディクロス
	public void bloodyCross(List<Player> player, List<Enemy> enemy) throws InterruptedException {
		mag=80;
		System.out.println(this.getName()+"『目障りだ、失せよ！』");
		Thread.sleep(1500);
		System.out.println(this.getName()+"の『ブラッディ・クロス！』");
		Thread.sleep(1500);
		System.out.println(this.getName()+"の槍が黒く輝き、漆黒の刃がほとばしる！");
		for(int c=0;c<player.size();c++){
			if(player.get(c).getHp()<=0){//もし対象が死んでいたら
				continue;//何もせず次へ
			}
			Avoidance=new java.util.Random().nextInt(100)+1;//回避の可否
			random=new java.util.Random().nextInt(31)+85;//ランダムで85～115を出す
			damage=(int)(((this.getAttack()+this.getMaAttack())*5)-(player.get(c).getDefense()*2.5)
					*0.5);
			damage=(int)(damage*mag/100)*random/100;//ダメージをさらにスキル倍率やランダム倍率
			critical=new java.util.Random().nextInt(101);//0～100でランダム抽選
			if(this.getCri()>=critical) {//もしキャラのクリティカルにマッチしていたら
				damage=damage*2;//ダメージ２倍
				System.out.print("クリティカル攻撃！");
				Thread.sleep(1500);
			}if(player.get(c).getCounter()>0) {//もしカウンター状態なら
				player.get(c).goCounter();
				this.setHp(this.getHp() - damage);//ターゲットのHPを減らす。
				System.out.println(this.getName()+"に"+damage+"のダメージ！");
				Thread.sleep(1500);
				player.get(c).setCounter(player.get(c).getCounter()-1);//カウンターを減らす
				continue;
			}else if(player.get(c).getAvoid()>=Avoidance&&this.getCri()<=critical) {//もしクリティカルじゃなければ回避可能
				player.get(c).toAvoidance();
				System.out.println(player.get(c).getName()+"は攻撃を回避した！");
				Thread.sleep(1500);
				continue;
			}
			damage=damage/10;//強すぎるので調整1/10
			player.get(c).setHp(player.get(c).getHp() - damage);//ターゲットのHPを減らす。
			System.out.println(player.get(c).getName()+"に"+damage+"のダメージ！");
			Thread.sleep(1500);
			if(player.get(c).getHp()<=0){
				player.get(target).setHp(0);
				System.out.println(player.get(c).getName()+"は戦闘不能になった！");
				Thread.sleep(1500);
			}
		}
	}

	/**
	 * @param player
	 * @param enemy
	 * @throws InterruptedException
	 */
	//薙ぎ払い
	public void cutOff(List<Player> player, List<Enemy> enemy) throws InterruptedException {//薙ぎ払いスキルメソッド
		mag=80;
		System.out.println(this.getName()+"『虫ケラが何人集まろうと無駄だ！』");
		Thread.sleep(1500);
		System.out.println(this.getName()+"の『薙ぎ払い』!");
		Thread.sleep(1500);
		System.out.println(this.getName()+"の槍による鋭い払い攻撃！");
		Thread.sleep(1500);
		for(int c=0;c<player.size();c++){
			if(player.get(c).getHp()<=0){//もし対象が死んでいたら
				continue;//何もせず次へ
			}
			Avoidance=new java.util.Random().nextInt(100)+1;//回避の可否
			random=new java.util.Random().nextInt(31)+85;//ランダムで85～115を出す
			damage=(int)((this.getAttack()*5)-(player.get(c).getDefense()*2.5)
					*0.5);
			damage=(int)(damage*mag/100)*random/100;//ダメージをさらにスキル倍率やランダム倍率
			critical=new java.util.Random().nextInt(101);//0～100でランダム抽選
			if(this.getCri()>=critical) {//もしキャラのクリティカルにマッチしていたら
				damage=damage*2;//ダメージ２倍
				System.out.print("クリティカル攻撃！");
				Thread.sleep(1500);
			}if(player.get(c).getCounter()>0) {//もしカウンター状態なら
				player.get(c).goCounter();
				this.setHp(this.getHp() - damage);//ターゲットのHPを減らす。
				System.out.println(this.getName()+"に"+damage+"のダメージ！");
				Thread.sleep(1500);
				player.get(c).setCounter(player.get(c).getCounter()-1);//カウンターを減らす
				continue;
			}else if(player.get(c).getAvoid()>=Avoidance&&this.getCri()<=critical) {//もしクリティカルじゃなければ回避可能
				player.get(c).toAvoidance();
				System.out.println(player.get(c).getName()+"は攻撃を回避した！");
				Thread.sleep(1500);
				continue;
			}
			damage=damage/10;//強すぎるので調整1/10
			player.get(c).setHp(player.get(c).getHp() - damage);//ターゲットのHPを減らす。
			System.out.println(player.get(c).getName()+"に"+damage+"のダメージ！");
			Thread.sleep(1500);
			if(player.get(c).getHp()<=0){
				player.get(target).setHp(0);
				System.out.println(player.get(c).getName()+"は戦闘不能になった！");
				Thread.sleep(1500);
			}
		}
	}



	/**
	 * @param name
	 */
	//コンストラクタ
	public BlackKnight(String name) {
		super("黒騎士");
		this.seteNum(0);
		this.setName("黒騎士");
		this.setLevel(87);
		this.setMaxHp(1000000);
		this.setMaxSp(98000);
		this.setHp(getMaxHp());
		this.setSp(getMaxSp());
		this.setAttack(1550);
		this.setMaAttack(2050);
		this.setDefense(2100);
		this.setMaDefense(1050);
		this.setCri(10);
		this.setAvoid(10);
		this.setSpeed(50);//最大100
		this.setTurn(0);
		this.setMaxArmourGuard(800);
		this.setArmourGuard(getMaxArmourGuard());//
		this.setAmmo(6);//弾薬
		//ここから特殊状態
		this.setLimitBreak(0);//100でリミットブレイク発動、特定行動で消費
		this.setExtension(false);//高揚状態。キャラごとの条件で発動、ステータス大幅アップ
		this.setArmorBreak(false);
		//ここから状態異常
		this.setFireDamage(false);
		this.setIceBind(false);
		this.setStan(false);
		this.setSilent(false);
		//黒騎士作製ここまで

	}




	public boolean isBattle1() {
		return battle1;
	}


	public void setBattle1(boolean battle1) {
		this.battle1 = battle1;
	}


	public boolean isBattle2() {
		return battle2;
	}

	public void setBattle2(boolean battle2) {
		this.battle2 = battle2;
	}
}
